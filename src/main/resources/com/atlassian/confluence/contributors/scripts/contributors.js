
jQuery(function($) {
    var Contributors = {
        init : function(contributorsDiv) {
            $(".show-hidden-contributors", contributorsDiv).click(function() {
                $(".hidden-contributor", contributorsDiv).removeClass("hidden");
                $(this).parent().remove();
                return false;
            });
        }
    };

    $("div.plugin-contributors").each(function() {
        Contributors.init($(this));
    })
});
