package com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.contributors.util.AuthorRanking;
import com.atlassian.confluence.contributors.util.AuthorRankingSystem;
import com.atlassian.confluence.contributors.util.PageDetailsHelper;
import com.atlassian.confluence.contributors.util.PageSearchHelper;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.RenderUtils;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

/**
 * @author Shannon Krebs
 */
public class ContributorsSummaryMacro extends BaseContributionMacro
{
    private static final String PARAMETER_COLUMNS = "columns";
    private static final String PARAMETER_GROUPBY = "groupby";
    private static final String PARAMETER_SHOWZEROCOUNTS = "showZeroCounts";

    private static final String ORDER_EDITS = "edits";
    private static final String ORDER_NAME = "name";
    private static final String ORDER_EDITTIME = "editTime";
    private static final String ORDER_UPDATE = "update";

    private static final String GROUPBY_PAGE = "pages";
    private static final String GROUPBY_CONTRIB = "contributors";

    private static final String COLUMNS_EDITS = "edits";
    private static final int COLUMNID_EDITS = 2;
    private static final String COLUMNS_EDITED = "edited";
    private static final int COLUMNID_EDITED = 3;

    private static final String COLUMNS_COMMENTS = "comments";
    private static final int COLUMNID_COMMENTS = 4;
    private static final String COLUMNS_COMMENTED = "commented";
    private static final int COLUMNID_COMMENTED = 5;

    private static final String COLUMNS_LABELS = "labels";
    private static final int COLUMNID_LABELS = 6;
    private static final String COLUMNS_LABELED = "labeled";
    private static final int COLUMNID_LABLED = 7;
    private static final String COLUMNS_LABELSLIST = "labellist";
    private static final int COLUMNID_LABELSLIST = 8;

    private static final String COLUMNS_WATCHES = "watches";
    private static final int COLUMNID_WATCHES = 9;
    private static final String COLUMNS_WATCHING = "watching";
    private static final int COLUMNID_WATCHING = 10;


    private static final String COLUMNS_LASTUPDATE = "lastupdate";
    private static final int COLUMNID_LASTUPDATE = 32;

    private static final Set COLUMNS = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(
                            COLUMNS_EDITS, COLUMNS_EDITED,
                            COLUMNS_COMMENTS, COLUMNS_COMMENTED,
                            COLUMNS_LABELS, COLUMNS_LABELED, COLUMNS_LABELSLIST,
                            COLUMNS_WATCHES, COLUMNS_WATCHING,
                            COLUMNS_LASTUPDATE
                    )
            )
    );

    private static final Map<String, Integer> COLUMN_NAMES_TO_ID_MAP = Collections.unmodifiableMap(
            new HashMap<String, Integer>()
            {
                {
                    put(COLUMNS_EDITS, COLUMNID_EDITS);
                    put(COLUMNS_EDITED, COLUMNID_EDITED);
                    put(COLUMNS_COMMENTS, COLUMNID_COMMENTS);
                    put(COLUMNS_COMMENTED, COLUMNID_COMMENTED);
                    put(COLUMNS_LABELS, COLUMNID_LABELS);
                    put(COLUMNS_LABELED, COLUMNID_LABLED);
                    put(COLUMNS_LABELSLIST, COLUMNID_LABELSLIST);
                    put(COLUMNS_WATCHES, COLUMNID_WATCHES);
                    put(COLUMNS_WATCHING, COLUMNID_WATCHING);
                    put(COLUMNS_LASTUPDATE, COLUMNID_LASTUPDATE);
                }
            }
    );

    private static final Random TABLE_ID_GENERATOR = new Random();
    
    private final SettingsManager settingsManager;

    private final PageSearchHelper pageSearchHelper;

    private final PageDetailsHelper pageDetailsHelper;

    public ContributorsSummaryMacro(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, SettingsManager settingsManager, PageSearchHelper pageSearchHelper, PageDetailsHelper pageDetailsHelper)
    {
        super(localeManager, i18NBeanFactory);
        this.settingsManager = settingsManager;
        this.pageSearchHelper = pageSearchHelper;
        this.pageDetailsHelper = pageDetailsHelper;
    }

    public String execute(Map parameters, String body, ConversionContext conversionContext) 
    {
        // Process the rest of the parameters and the contributor statistics
        // only if the macro is on an instance of a page
        ContentEntityObject contentObject = conversionContext.getEntity();
        if (contentObject instanceof AbstractPage)
        {
            @SuppressWarnings("unchecked")
            Map<String, String> _parameters = (Map<String, String>) parameters;

            Collection<String> columnNames = Collections2.filter(
                    new LinkedHashSet<String>(Arrays.asList(
                            StringUtils.split(
                                    StringUtils.defaultString(
                                            _parameters.get(PARAMETER_COLUMNS)
                                            , StringUtils.join(Arrays.asList(COLUMNS_EDITS, COLUMNS_COMMENTS, COLUMNS_LABELS), ",")
                                    ),
                                    ","
                            )
                    )),
                    new Predicate<String>()
                    {
                        public boolean apply(String columnName)
                        {
                            return COLUMNS.contains(columnName);
                        }
                    }
            );
            Collection<Integer> columnIds = Collections2.transform(
                    columnNames,
                    new Function<String, Integer>()
                    {
                        public Integer apply(String columnNames)
                        {
                            return COLUMN_NAMES_TO_ID_MAP.get(columnNames);
                        }
                    }
            );

            int groupByType = StringUtils.equals(GROUPBY_PAGE, _parameters.get(PARAMETER_GROUPBY))
                    ? PageDetailsHelper.GROUPBY_PAGES
                    : PageDetailsHelper.GROUPBY_CONTRIBUTORS;
            StringBuilder tableHeaderBuilder = new StringBuilder("<table id='").append(generateTableId())
                    .append("' summary='").append(getText("summary.table.summary"))
                    .append("' class='aui'><tbody><tr><th>")
                    .append(getText(PageDetailsHelper.GROUPBY_PAGES == groupByType ? "summary.tableheader.page" : "summary.tableheader.user"))
                    .append("</th>");

            for (int columnId : columnIds)
            {
                tableHeaderBuilder.append("<th>");
                switch (columnId)
                {
                    case COLUMNID_COMMENTS:
                        tableHeaderBuilder.append(getText("summary.tableheader.comments"));
                        break;
                    case COLUMNID_COMMENTED:
                        tableHeaderBuilder.append(getText("summary.tableheader.commented"));
                        break;
                    case COLUMNID_EDITS:
                        tableHeaderBuilder.append(getText("summary.tableheader.edits"));
                        break;
                    case COLUMNID_EDITED:
                        tableHeaderBuilder.append(getText("summary.tableheader.edited"));
                        break;
                    case COLUMNID_LABELS:
                        tableHeaderBuilder.append(getText("summary.tableheader.labels"));
                        break;
                    case COLUMNID_LABLED:
                        tableHeaderBuilder.append(getText("summary.tableheader.labelled"));
                        break;
                    case COLUMNID_LABELSLIST:
                        tableHeaderBuilder.append(getText("summary.tableheader.labellist"));
                        break;
                    case COLUMNID_LASTUPDATE:
                        tableHeaderBuilder.append(getText("summary.tableheader.lastupdate"));
                        break;
                    case COLUMNID_WATCHES:
                        tableHeaderBuilder.append(getText("summary.tableheader.watches"));
                        break;
                    case COLUMNID_WATCHING:
                        tableHeaderBuilder.append(getText("summary.tableheader.watching"));
                        break;
                    default:

                }
                tableHeaderBuilder.append("</th>");
            }

            StringBuilder outputBuilder = new StringBuilder();
            outputBuilder.append(tableHeaderBuilder + "</tr>");

            AbstractPage abstractPage = (AbstractPage) contentObject;

            AuthorRankingSystem rankingSystem = getRankingSystem(_parameters);
            pageDetailsHelper.processPageList(
                    pageSearchHelper.pageList(
                            getPage(_parameters, abstractPage),
                            getSpaceKey(_parameters, abstractPage),
                            getLabelsString(_parameters),
                            getScope(_parameters),
                            getPublishedDate(_parameters, abstractPage),
                            getContentType(_parameters)),
                    rankingSystem,
                    groupByType);


            boolean startValue = BooleanUtils.toBoolean(_parameters.get(PARAMETER_SHOWZEROCOUNTS));
            List rankedAuthors = rankingSystem.getRankedAuthors(
                    isReverse(_parameters),
                    startValue || columnNames.contains(COLUMNS_EDITS) || columnNames.contains(COLUMNS_EDITED) || columnNames.contains(COLUMNS_LASTUPDATE),
                    startValue || columnNames.contains(COLUMNS_COMMENTS) || columnNames.contains(COLUMNS_COMMENTED) || columnNames.contains(COLUMNS_LASTUPDATE),
                    startValue || columnNames.contains(COLUMNS_LABELS) || columnNames.contains(COLUMNS_LABELED) || columnNames.contains(COLUMNS_LABELSLIST) || columnNames.contains(COLUMNS_LASTUPDATE),
                    startValue || columnNames.contains(COLUMNS_WATCHES) || columnNames.contains(COLUMNS_WATCHING) || columnNames.contains(COLUMNS_LASTUPDATE));


            Iterator iAuthors = rankedAuthors.iterator();
            StringBuilder linkPrefixBuffer = new StringBuilder();
            StringBuilder userColumnBuffer = new StringBuilder();
            String linkPrefix;
            String linkPostfix = "</a> ";

            // Build the table rows
            int limit = getLimit(_parameters);
            String baseURL = settingsManager.getGlobalSettings().getBaseUrl();
            boolean showAnonymous = isShowAnonymousContributions(_parameters);

            for (int count = 0; iAuthors.hasNext() && ((count < limit) || isLimitLess(limit)); ++count)
            {
                AuthorRanking author = (AuthorRanking) iAuthors.next();

                linkPrefixBuffer.setLength(0);
                linkPrefix = linkPrefixBuffer.append("<a href=\"").append(baseURL).toString();

                // User-Page Column
                userColumnBuffer.setLength(0);
                userColumnBuffer.append("<tr><td>");

                if (PageDetailsHelper.GROUPBY_PAGES == groupByType)
                {
                    userColumnBuffer
                            .append(linkPrefix).append(author.getIdString())
                            .append("\">")
                            .append(author.getFullNameString())
                            .append(linkPostfix);

                    linkPrefix = linkPrefixBuffer.append("/display/~").toString();
                }
                else
                {
                    if (PageDetailsHelper.ANONYMOUS_USER.equals(author.getIdString()))
                    {
                        if (showAnonymous)
                            userColumnBuffer.append(PageDetailsHelper.ANONYMOUS_USER);
                    }
                    else
                    {
                        userColumnBuffer
                                .append(linkPrefix)
                                .append("/display/~")
                                .append(author.getIdString())
                                .append("\">")
                                .append(GeneralUtil.htmlEncode(author.getFullNameString()))
                                .append(linkPostfix);
                    }
                }
                userColumnBuffer.append("</td>");

                outputBuilder.append(userColumnBuffer);


                // foreach of the specified columns
                for (int columnId : columnIds)
                {

                    outputBuilder.append("<td>");

                    switch (columnId)
                    {

                        // Comment Count Column
                        case COLUMNID_COMMENTS:
                            outputBuilder.append(author.getComments());
                            break;

                        // Commented Column
                        case COLUMNID_COMMENTED:
                            Iterator cItr = author.getCommentMap().entrySet().iterator();
                            while (cItr.hasNext())
                            {
                                Map.Entry entry = (Map.Entry) cItr.next();

                                outputBuilder.append(createCellText(entry, linkPrefix, linkPostfix, showAnonymous));

                            }
                            break;

                        // Edit Count Column
                        case COLUMNID_EDITS:
                            outputBuilder.append(author.getEdits());
                            break;

                        // Edited Coulumn
                        case COLUMNID_EDITED:
                            Iterator eItr = author.getEditMap().entrySet().iterator();
                            while (eItr.hasNext())
                            {
                                Map.Entry entry = (Map.Entry) eItr.next();
                                outputBuilder.append(createCellText(entry, linkPrefix, linkPostfix, showAnonymous));

                            }
                            break;

                        // Label Count Coulumn
                        case COLUMNID_LABELS:
                            outputBuilder.append(author.getLabels());
                            break;

                        // Labeled Column
                        case COLUMNID_LABLED:
                            Iterator lItr = author.getLabelMap().entrySet().iterator();
                            while (lItr.hasNext())
                            {
                                Map.Entry entry = (Map.Entry) lItr.next();
                                outputBuilder.append(createCellText(entry, linkPrefix, linkPostfix, showAnonymous));
                            }
                            break;

                        // Label List Column
                        case COLUMNID_LABELSLIST:
                            Iterator itr = author.getLabelList().iterator();
                            while (itr.hasNext())
                            {
                                String label = (String) itr.next();
                                outputBuilder.append("<a href=\"").append(baseURL).append("/label/").append(label).append("\">").append(label).append(linkPostfix);

                            }
                            break;

                        // Last Update Column
                        case COLUMNID_LASTUPDATE:
                            if (author.getLastActiveTime() > 0)
                            {
                                outputBuilder.append(GeneralUtil.getRelativeTime(author.getLastActiveDate()));
                            }
                            break;

                        //	Watch Count column
                        case COLUMNID_WATCHES:
                            outputBuilder.append(author.getWatches());
                            break;

                        // Watching Column
                        case COLUMNID_WATCHING:
                            Iterator wItr = author.getWatchMap().entrySet().iterator();
                            while (wItr.hasNext())
                            {
                                Map.Entry entry = (Map.Entry) wItr.next();
                                outputBuilder.append(createCellText(entry, linkPrefix, linkPostfix, showAnonymous));

                            }

                        default:

                    }
                    outputBuilder.append("</td>");

                }

                outputBuilder.append("</tr>");
            }
            // End build table rows
            return outputBuilder.append("</tbody></table>").toString();
        }
        else if (contentObject instanceof Comment)
        {
            return execute(parameters, body, ((Comment) contentObject).getOwner().toPageContext());
        }

        if (contentObject instanceof Draft)
            return RenderUtils.blockError(getText("error.preview", Arrays.asList(getText("com.atlassian.confluence.contributors.contributors-summary.label"))), "");
        else
            return RenderUtils.blockError(getText("error.unsupportedcontent", Arrays.asList(getText("com.atlassian.confluence.contributors.contributors-summary.label"))), "");
    }

    private StringBuilder createCellText(Entry entry, String linkPrefix, String linkPostfix, boolean showAnonymous)
    {
        StringBuilder cellTextBuilder = new StringBuilder();
        if (entry.getValue().equals(PageDetailsHelper.ANONYMOUS_USER))
        {
            if (showAnonymous)
            {
                cellTextBuilder.append(PageDetailsHelper.ANONYMOUS_USER).append(" ");
            }
        }
        else
        {
            cellTextBuilder.append(linkPrefix).append(entry.getKey()).append("\">").append(entry.getValue()).append(linkPostfix);
        }

        return cellTextBuilder;
    }

    private AuthorRankingSystem getRankingSystem(Map<String, String> parameters)
    {
        String orderBy = getOrder(parameters);
        AuthorRankingSystem rankingSystem;

        if (ORDER_NAME.equalsIgnoreCase(orderBy))
            rankingSystem = new AuthorRankingSystem(AuthorRankingSystem.RankType.FULL_NAME);
        else if (ORDER_EDITTIME.equalsIgnoreCase(orderBy))
            rankingSystem = new AuthorRankingSystem(AuthorRankingSystem.RankType.EDIT_TIME);
        else if (ORDER_UPDATE.equalsIgnoreCase(orderBy))
            rankingSystem = new AuthorRankingSystem(AuthorRankingSystem.RankType.LASTACTIVE_TIME);
        else
            rankingSystem = new AuthorRankingSystem(AuthorRankingSystem.RankType.EDIT_COUNT);

        return rankingSystem;
    }

    protected String generateTableId()
    {
        return "contributors-summary-" + TABLE_ID_GENERATOR.nextInt(Integer.MAX_VALUE);
    }
}
