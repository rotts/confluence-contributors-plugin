package com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.contributors.util.AuthorRankingSystem;
import com.atlassian.confluence.contributors.util.PageDetailsHelper;
import com.atlassian.confluence.contributors.util.PageSearchHelper;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.RenderUtils;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * @author Brock Janiczak
 * @author Shannon Krebs
 */
public class ContributorsMacro extends BaseContributionMacro
{

    // Display Parameters
    private static final String PARAMETER_MODE = "mode";
    private static final String PARAMETER_SHOW_COUNT = "showCount";
    private static final String PARAMETER_INCLUDE = "include";
    private static final String PARAMETER_SHOWTIME = "showLastTime";

    // Search Parameters

    // Advanced Paramaters
    private static final String PARAMETER_SHOWPAGES = "showPages";
    private static final String PARAMETER_ERRORSTRING = "noneFoundMessage";


    private static final String ORDER_COUNT = "count";
    private static final String ORDER_NAME = "name";
    private static final String ORDER_UPDATE = "update";

    private static final String MODE_INLINE = "inline";
    private static final String MODE_LIST = "list";

    private static final String INCLUDE_AUTHORS = "authors";
    private static final String INCLUDE_COMMENTS = "comments";
    private static final String INCLUDE_LABELS = "labels";
    private static final String INCLUDE_WATCHES = "watches";

    private final PageSearchHelper pageSearchHelper;

    private final PageDetailsHelper pageDetailsHelper;

    private final VelocityHelperService velocityHelperService;

    public ContributorsMacro(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, PageSearchHelper pageSearchHelper, PageDetailsHelper pageDetailsHelper, VelocityHelperService velocityHelperService)
    {
        super(localeManager, i18NBeanFactory);
        this.pageSearchHelper = pageSearchHelper;
        this.pageDetailsHelper = pageDetailsHelper;
        this.velocityHelperService = velocityHelperService;
    }

    public String execute(Map parameters, String body, ConversionContext conversionContext)
    {
        ContentEntityObject contentObject = conversionContext.getEntity();

        if (contentObject instanceof AbstractPage)
        {
            @SuppressWarnings("unchecked")
            Map<String, String> _parameters = (Map<String, String>) parameters;
            AbstractPage abstractPage = (AbstractPage) contentObject;

            List<AbstractPage> pageList = pageSearchHelper.pageList(
                    getPage(_parameters, abstractPage),
                    getSpaceKey(_parameters, abstractPage),
                    getLabelsString(_parameters),
                    getScope(_parameters),
                    getPublishedDate(_parameters, abstractPage),
                    getContentType(_parameters));

            AuthorRankingSystem rankingSystem = getRankingSystem(_parameters);

            // Work out what fields should be counted (if any)
            Collection<String> includes = new HashSet<String>(Arrays.asList(StringUtils.split(StringUtils.defaultString(_parameters.get(PARAMETER_INCLUDE), INCLUDE_AUTHORS), ", ")));
            pageDetailsHelper.processPageList(
                    pageList,
                    rankingSystem,
                    PageDetailsHelper.GROUPBY_CONTRIBUTORS,
                    includes.contains(INCLUDE_AUTHORS),
                    includes.contains(INCLUDE_COMMENTS),
                    includes.contains(INCLUDE_LABELS),
                    includes.contains(INCLUDE_WATCHES));

            List allUsers = rankingSystem.getRankedAuthors(isReverse(_parameters));

            // Remove the anonymous user if not wanted
            if (!isShowAnonymousContributions(_parameters))
                allUsers.remove(rankingSystem.getAuthorRanking(PageDetailsHelper.ANONYMOUS_USER));

            String returnText = "";

            // Debug output to show what pages were returned from the search
            if (BooleanUtils.toBoolean(_parameters.get(PARAMETER_SHOWPAGES)))
            {
                returnText += new StringBuilder("<p>").append(
                        StringUtils.join(
                                Collections2.transform(
                                        pageList,
                                        new Function<AbstractPage, String>()
                                        {
                                            public String apply(AbstractPage abstractPage)
                                            {
                                                return abstractPage.getTitle();
                                            }
                                        }
                                )
                                , ", "
                        )
                ).append("</p>").toString();
            }

            // Divide the users between the visible and hidden list
            if (allUsers.size() > 0)
            {
                List visibleUsers;
                List hiddenUsers;

                int limit = getLimit(_parameters);
                if (!isLimitLess(limit) && limit < allUsers.size())
                {
                    visibleUsers = allUsers.subList(0, limit);
                    hiddenUsers = allUsers.subList(limit, allUsers.size());
                }
                else
                {
                    visibleUsers = allUsers;
                    hiddenUsers = Collections.emptyList();
                }

                Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
                velocityContext.put("visibleContributors", visibleUsers);
                velocityContext.put("hiddenContributors", hiddenUsers);
                velocityContext.put("showCount", BooleanUtils.toBoolean(_parameters.get(PARAMETER_SHOW_COUNT)));
                velocityContext.put("showTime", BooleanUtils.toBoolean(_parameters.get(PARAMETER_SHOWTIME)));

                returnText += velocityHelperService.getRenderedTemplate(
                        StringUtils.equals(MODE_LIST, _parameters.get(PARAMETER_MODE))
                                ? "com/atlassian/confluence/contributors/templates/contributors-list.vm"
                                : "com/atlassian/confluence/contributors/templates/contributors-flat.vm",
                        velocityContext);

            }
            else
            {
                // No users were found so return an error message
                returnText = StringUtils.defaultIfEmpty(
                        GeneralUtil.htmlEncode(_parameters.get(PARAMETER_ERRORSTRING)),
                        String.format("No contributors found for: %s on selected page(s)", GeneralUtil.htmlEncode(StringUtils.join(includes, ", ")))
                );
            }

            return returnText;
        }
        else if (contentObject instanceof Comment)
        {
            return execute(parameters, body, ((Comment) contentObject).getOwner().toPageContext());
        }

        if (contentObject instanceof Draft)
            return RenderUtils.blockError(getText("error.preview", Arrays.asList(getText("com.atlassian.confluence.contributors.contributors.label"))), "");
        else
            return RenderUtils.blockError(getText("error.unsupportedcontent", Arrays.asList(getText("com.atlassian.confluence.contributors.contributors.label"))), "");
    }

    private AuthorRankingSystem getRankingSystem(Map<String, String> parameters)
    {
        String order = getOrder(parameters);

        return StringUtils.equals(ORDER_NAME, order)
                ? new AuthorRankingSystem(AuthorRankingSystem.RankType.FULL_NAME)
                : (StringUtils.equals(ORDER_UPDATE, order)
                ? new AuthorRankingSystem(AuthorRankingSystem.RankType.LASTACTIVE_TIME)
                : new AuthorRankingSystem(AuthorRankingSystem.RankType.TOTAL_COUNT)
        );
    }
}
