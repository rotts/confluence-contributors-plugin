package com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

public abstract class BaseContributionMacro extends BaseMacro implements Macro
{
    private static final Logger LOG = LoggerFactory.getLogger(BaseContributionMacro.class);

    private static final int NO_LIMIT = -1;

	private static final String PARAMETER_LIMIT = "limit";
	private static final String PARAMETER_REVERSE = "reverse";
	private static final String PARAMETER_SHOWANON = "showAnonymous";
	private static final String PARAMETER_ORDER = "order";

    /* Search parameters */
	private static final String PARAMETER_SPACE = "space";
	private static final String PARAMETER_SPACES = "spaces";
	private static final String PARAMETER_PAGE = "page";
	private static final String PARAMETER_LABELS = "labels";
	private static final String PARAMETER_LABEL = "label";
	private static final String PARAMETER_PUBLISHDATE = "publishDate";
	private static final String PARAMETER_SCOPE = "scope";
    private static final String PARAMETER_CONTENTTYPE = "contentType";

    private final LocaleManager localeManager;

    private final I18NBeanFactory i18NBeanFactory;

    protected BaseContributionMacro(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory)
    {
        this.localeManager = localeManager;
        this.i18NBeanFactory = i18NBeanFactory;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    int getLimit(Map<String, String> parameters)
    {
        int limit = NO_LIMIT;
        try
        {
            limit = Math.max(Integer.parseInt(StringUtils.defaultString(parameters.get(PARAMETER_LIMIT))), limit);
        }
        catch (NumberFormatException e)
        {
            LOG.debug(String.format("Invalid limit specified %s", parameters.get(PARAMETER_LIMIT)), e);
        }

        return limit;
    }

    boolean isLimitLess(int limitValue)
    {
        return limitValue <= NO_LIMIT;
    }

    String getSpaceKey(Map<String, String> parameters, AbstractPage abstractPage)
    {
        String spaceKey = getSpaceKeyFromParametersOnly(parameters);
        if (StringUtils.isBlank(spaceKey))
            spaceKey = getLatestVersion(abstractPage).getSpaceKey(); /* CONF-45  If page is not the newest, use the space key of the newest version */

        return spaceKey;
    }

    private AbstractPage getLatestVersion(AbstractPage abstractPage)
    {
        return abstractPage.isLatestVersion() ? abstractPage : abstractPage.getLatestVersion();
    }

    private String getSpaceKeyFromParametersOnly(Map<String, String> parameters)
    {
        return StringUtils.defaultIfEmpty(parameters.get(PARAMETER_SPACES), parameters.get(PARAMETER_SPACE));
    }

    String getPage(Map<String, String> parameters, AbstractPage abstractPage)
    {
        String page = parameters.get(PARAMETER_PAGE);

        if (StringUtils.isBlank(page) && StringUtils.isBlank(getSpaceKeyFromParametersOnly(parameters)) && StringUtils.isBlank(getLabelsString(parameters)))
            return getLatestVersion(abstractPage).getTitle(); /* CONF-45  If page is not the newest, use the space key of the newest version */

        return page;
    }

    String getLabelsString(Map<String, String> parameters)
    {
        return StringUtils.defaultIfEmpty(parameters.get(PARAMETER_LABELS), parameters.get(PARAMETER_LABEL));
    }

    String getScope(Map<String, String> parameters)
    {
        return parameters.get(PARAMETER_SCOPE);
    }

    String getContentType(Map<String, String> parameters)
    {
        return parameters.get(PARAMETER_CONTENTTYPE);
    }

    Calendar getPublishedDate(Map<String, String> parameters, AbstractPage abstractPage)
    {
        Calendar publishedDate = Calendar.getInstance();
        
        try
        {
            publishedDate.setTimeInMillis(DateTimeFormat.forPattern("yyyy/MM/dd").parseDateTime(StringUtils.defaultString(parameters.get(PARAMETER_PUBLISHDATE))).getMillis());
        }
        catch (IllegalArgumentException invalidDate)
        {
            LOG.debug("Invalid publishDate specified. Trying to fallback on content creation date", invalidDate);
            if (abstractPage instanceof BlogPost)
                publishedDate.setTimeInMillis(abstractPage.getCreationDate().getTime());
            else
                publishedDate = null;
        }

        return publishedDate;
    }

    boolean isReverse(Map<String, String> parameters)
    {
        return BooleanUtils.toBoolean(parameters.get(PARAMETER_REVERSE));
    }

    boolean isShowAnonymousContributions(Map<String, String> parameters)
    {
        return BooleanUtils.toBoolean(parameters.get(PARAMETER_SHOWANON));
    }

    String getOrder(Map<String, String> parameters)
    {
        return parameters.get(PARAMETER_ORDER);
    }

    public String execute(Map parameters, String body, RenderContext renderContext)
    {
        return execute(parameters, body, new DefaultConversionContext(renderContext));
    }

    public abstract String execute(Map<String, String> parameters, String body, ConversionContext conversionContext);

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    String getText(String key, List<?> substitutions)
    {
        return getI18NBean().getText(key, substitutions);
    }

    String getText(String key)
    {
        return getI18NBean().getText(key);
    }
}
