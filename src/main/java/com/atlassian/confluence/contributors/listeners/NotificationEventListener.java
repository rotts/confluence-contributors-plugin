package com.atlassian.confluence.contributors.listeners;

import com.atlassian.confluence.event.events.content.mail.notification.ContentNotificationAddedEvent;
import com.atlassian.confluence.event.events.content.mail.notification.ContentNotificationEvent;
import com.atlassian.confluence.event.events.content.mail.notification.ContentNotificationRemovedEvent;
import com.atlassian.confluence.event.events.content.mail.notification.SpaceNotificationAddedEvent;
import com.atlassian.confluence.event.events.content.mail.notification.SpaceNotificationEvent;
import com.atlassian.confluence.event.events.content.mail.notification.SpaceNotificationRemovedEvent;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.search.ConfluenceIndexer;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;

public class NotificationEventListener implements EventListener
{
    private final ConfluenceIndexer indexer;

    public NotificationEventListener(ConfluenceIndexer indexer)
    {
        this.indexer = indexer;
    }

    private static final Class[] HANDLED_CLASSES = new Class[] {
            ContentNotificationAddedEvent.class,
            ContentNotificationRemovedEvent.class,
            SpaceNotificationAddedEvent.class,
            SpaceNotificationRemovedEvent.class
    };

    public void handleEvent(final Event event)
    {
        if (event instanceof ContentNotificationEvent)
        {
            final ContentNotificationEvent contentNotificationEvent = (ContentNotificationEvent) event;
            final AbstractPage pageToBeReindexed = contentNotificationEvent.getPage();

            indexer.reIndex(pageToBeReindexed);
        }
        else if (event instanceof SpaceNotificationEvent)
        {
            final SpaceNotificationEvent spaceNotificationEvent = (SpaceNotificationEvent) event;
            final Space spaceToBeReindexed = spaceNotificationEvent.getSpace();

            indexer.reIndex(spaceToBeReindexed);
        }
    }

    public Class[] getHandledEventClasses()
    {
        return HANDLED_CLASSES;
    }
}
