package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.core.persistence.ContentEntityObjectDao;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.user.ConfluenceUser;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;


public class AuthorContributionExtractor extends AbstractContributionExtractor
{
    private static final Logger logger = LoggerFactory.getLogger(AuthorContributionExtractor.class);

    public static final String AUTHOR_CONTRIBUTIONS = "authorContributions";

    public static final String ANONYMOUS_USER_NAME = "Anonymous";

    private ContentEntityObjectDao contentEntityObjectDao;

    public void setContentEntityObjectDao(ContentEntityObjectDao contentEntityObjectDao)
    {
        this.contentEntityObjectDao = contentEntityObjectDao;
    }

    @Override
    public Collection<FieldDescriptor> extractFields(final Object searchable)
    {
        if (searchable instanceof AbstractPage)
        {
            final AbstractPage abstractPage = (AbstractPage) searchable;
            final List<VersionHistorySummary> versionHistories = contentEntityObjectDao.getVersionHistorySummary(abstractPage.getId());
            final StringBuilder lastModifiersAndDatePairsBuffer = new StringBuilder();


            for (final VersionHistorySummary versionHistory : versionHistories)
            {
                if (null == versionHistory.getLastModificationDate())
                {
                    if (logger.isWarnEnabled())
                        logger.warn(new StringBuffer("Page#")
                                .append(abstractPage.getId())
                                .append(" version#")
                                .append(versionHistory.getVersion())
                                .append(" has a null last modification date and will be dropped from the content edit contribution calculation.")
                                .toString());
                    continue;
                }

                ConfluenceUser lastModifier = versionHistory.getLastModifier();
                lastModifiersAndDatePairsBuffer
                        .append(StringEscapeUtils.escapeHtml(lastModifier != null ? lastModifier.getKey().getStringValue() : null))
                        .append(CONTRIBUTION_TOKEN_SEPARATOR)
                        .append(versionHistory.getLastModificationDate().getTime())
                        .append(SystemUtils.LINE_SEPARATOR);
            }

            return ImmutableList.of(new FieldDescriptor(AUTHOR_CONTRIBUTIONS, lastModifiersAndDatePairsBuffer.toString(),
                    FieldDescriptor.Store.YES, FieldDescriptor.Index.NOT_ANALYZED));
        }

        return null;
    }
}
