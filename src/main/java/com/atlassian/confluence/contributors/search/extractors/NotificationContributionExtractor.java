package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

import java.util.Collection;
import java.util.HashSet;

public class NotificationContributionExtractor extends AbstractContributionExtractor
{
    public static final String WATCHERS = "watchers";

    private final NotificationManager notificationManager;

    public NotificationContributionExtractor(NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
    }

    @Override
    public Collection<FieldDescriptor> extractFields(final Object searchable)
    {
        final String name = "NotificationContributionExtractor::addField(Document, StringBuffer, Searchable):void";

        if (searchable instanceof Space)
        {
            final Collection<String> userKeys;

            try
            {
                UtilTimerStack.push(name);

                userKeys = Collections2.transform(
                        notificationManager.getNotificationsBySpaceAndType(((Space) searchable), null),
                        new Function<Notification, String>()
                        {
                            public String apply(Notification notification)
                            {
                                return notification.getReceiver().getKey().getStringValue();
                            }
                        });

                return ImmutableList.of(getFieldDescriptor(userKeys));
            }
            finally
            {
                UtilTimerStack.pop(name);
            }
        }
        else if (searchable instanceof AbstractPage)
        {
            final Collection<String> userNames;

            try
            {
                UtilTimerStack.push(name);

                userNames = Collections2.transform(
                        notificationManager.getNotificationsByPage((AbstractPage) searchable),
                        new Function<Notification, String>()
                        {
                            public String apply(Notification notification)
                            {
                                return notification.getReceiver().getKey().getStringValue();
                            }
                        });

                return ImmutableList.of(getFieldDescriptor(userNames));
            }
            finally
            {
                UtilTimerStack.pop(name);
            }
        }

        return null;
    }

    private FieldDescriptor getFieldDescriptor(final Collection<String> userKeys)
    {
        return new FieldDescriptor(WATCHERS, StringUtils.join(new HashSet<String>(userKeys), SystemUtils.LINE_SEPARATOR),
                FieldDescriptor.Store.YES, FieldDescriptor.Index.NOT_ANALYZED);
    }
}
