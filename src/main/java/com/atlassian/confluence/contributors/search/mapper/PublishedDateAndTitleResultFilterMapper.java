package com.atlassian.confluence.contributors.search.mapper;

import com.atlassian.confluence.search.v2.lucene.LuceneResultFilterMapper;
import com.atlassian.confluence.search.v2.lucene.LuceneResultFilter;
import com.atlassian.confluence.search.v2.ResultFilter;
import com.atlassian.confluence.contributors.search.filter.PublishedDateAndTitleResultFilter;
import com.atlassian.confluence.contributors.search.lucene.PublishedDateAndTitleLuceneResultFilter;
import com.atlassian.confluence.contributors.search.lucene.ContentTitleLuceneResultFilter;
import com.atlassian.confluence.contributors.search.lucene.BlogPostPublishedDateLuceneResultFilter;

public class PublishedDateAndTitleResultFilterMapper implements LuceneResultFilterMapper
{
    public LuceneResultFilter convertToLuceneResultFilter(final ResultFilter resultFilter) 
    {
        final PublishedDateAndTitleResultFilter publishedDateAndTitleResultFilter =
                (PublishedDateAndTitleResultFilter) resultFilter;

        return new PublishedDateAndTitleLuceneResultFilter(
                new ContentTitleLuceneResultFilter(publishedDateAndTitleResultFilter.getTitle()),
                new BlogPostPublishedDateLuceneResultFilter(
                        publishedDateAndTitleResultFilter.getPublishedDate(),
                        publishedDateAndTitleResultFilter.getPublishedDateField())
        );
    }
}
