package com.atlassian.confluence.contributors.search.filter;

import com.atlassian.confluence.search.v2.ResultFilter;

import java.util.List;
import java.util.Calendar;
import java.util.Arrays;

public class PublishedDateAndTitleResultFilter implements ResultFilter
{
    public static final String KEY = "publishedDateAndTitle";

    private final String title;

    private final Calendar publishedDate;

    private final int publishedDateField;

    public PublishedDateAndTitleResultFilter(final String title, final Calendar publishedDate, final int publishedDateField)
    {
        this.title = title;
        this.publishedDate = publishedDate;
        this.publishedDateField = publishedDateField;
    }

    public String getTitle()
    {
        return title;
    }

    public Calendar getPublishedDate()
    {
        return publishedDate;
    }

    public int getPublishedDateField()
    {
        return publishedDateField;
    }

    public String getKey()
    {
        return KEY;
    }

    public List getParameters()
    {
        return Arrays.asList(new Object[] {
                title,
                publishedDate,
                publishedDateField
        });
    }

    ///CLOVER:OFF
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PublishedDateAndTitleResultFilter that = (PublishedDateAndTitleResultFilter) o;

        if (publishedDateField != that.publishedDateField) return false;
        if (publishedDate != null ? !publishedDate.equals(that.publishedDate) : that.publishedDate != null)
            return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (title != null ? title.hashCode() : 0);
        result = 31 * result + (publishedDate != null ? publishedDate.hashCode() : 0);
        result = 31 * result + publishedDateField;
        return result;
    }
    ///CLOVER:ON
}
