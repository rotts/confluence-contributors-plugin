package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.google.common.collect.ImmutableList;

import java.util.Collection;

public class CommentContributionExtractor extends AbstractContributionExtractor
{
    public static final String CONTAINING_PAGE_ID = "containingPageId";

    @Override
    public Collection<FieldDescriptor> extractFields(final Object searchable)
    {
        if (searchable instanceof Comment)
        {
            AbstractPage page;

            try
            {
                ContentEntityObject owner = ((Comment) searchable).getOwner();
                if (!(owner instanceof AbstractPage))
                {
                    //this comment is attached to something other than a page. Ignore.
                    return null;
                }

                page = (AbstractPage) owner;
            }
            catch (IllegalStateException ise)
            {
                //this comment is attached to something other than a page. Ignore.
                return null;
            }

            return ImmutableList.of(new FieldDescriptor(CONTAINING_PAGE_ID, page.getIdAsString(),
                    FieldDescriptor.Store.YES, FieldDescriptor.Index.NOT_ANALYZED));
        }

        return null;
    }
}
