package com.atlassian.confluence.contributors.search.lucene;

import com.atlassian.confluence.search.v2.lucene.LuceneResultFilter;
import com.atlassian.confluence.search.v2.lucene.WrappedHit;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.index.CorruptIndexException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class ContentTitleLuceneResultFilter implements LuceneResultFilter
{
    private static final Logger logger = LoggerFactory.getLogger(ContentTitleLuceneResultFilter.class);

    private final String contentTitle;

    public ContentTitleLuceneResultFilter(final String contentTitle)
    {
        this.contentTitle = contentTitle;
    }

    public boolean isIncludedInResults(WrappedHit wrappedHit)
    {
        try
        {
            return StringUtils.isBlank(contentTitle) || StringUtils.equalsIgnoreCase(contentTitle, wrappedHit.get("title"));
        }
        catch (final CorruptIndexException cie)
        {
            logger.error("Index corruption detected while getting \"title\" from wrapped hit: " + wrappedHit, cie);
        }
        catch (final IOException ioe)
        {
            logger.error("IO error while getting \"title\" from wrapped hit: " + wrappedHit, ioe);
        }

        return false; /* Should not include -- defensive if there is an error */
    }

    public boolean shouldContinueIterating()
    {
        return true;
    }

    public int getExpectedResultsCount()
    {
        return -1; /* no hints */
    }
}
