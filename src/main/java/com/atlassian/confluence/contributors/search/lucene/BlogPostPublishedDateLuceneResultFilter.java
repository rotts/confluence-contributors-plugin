package com.atlassian.confluence.contributors.search.lucene;

import com.atlassian.bonnie.LuceneUtils;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.search.v2.lucene.LuceneResultFilter;
import com.atlassian.confluence.search.v2.lucene.WrappedHit;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.index.CorruptIndexException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Calendar;


public class BlogPostPublishedDateLuceneResultFilter implements LuceneResultFilter
{
    private static final Logger logger = LoggerFactory.getLogger(BlogPostPublishedDateLuceneResultFilter.class);

    private final Calendar publishedDate;

    private final int field;

    public BlogPostPublishedDateLuceneResultFilter(final Calendar publishedDate, int field)
    {
        if (null != publishedDate)
        {
            this.publishedDate = Calendar.getInstance();
            this.publishedDate.setTimeInMillis(publishedDate.getTimeInMillis());
        }
        else
        {
            this.publishedDate = null;
        }
        this.field = field;
    }

    public boolean isIncludedInResults(WrappedHit wrappedHit)
    {
        try
        {
            if (null != publishedDate && StringUtils.equals(BlogPost.CONTENT_TYPE, wrappedHit.get("type")))
            {
                final Calendar _publishedDate =  Calendar.getInstance();

                _publishedDate.setTime(LuceneUtils.stringToDate(wrappedHit.get("created")));
                return publishedDate.get(field) == _publishedDate.get(field);
            }

            return true;
        }
        catch (final CorruptIndexException cie)
        {
            logger.error("Index corruption detected while getting \"created\" or \"type\" from wrapped hit: " + wrappedHit, cie);
        }
        catch (final IOException ioe)
        {
            logger.error("IO error while getting \"created\" or \"type\" from wrapped hit: " + wrappedHit, ioe);
        }

        return false; /* Should not include -- defensive if there is an error */
    }

    public boolean shouldContinueIterating()
    {
        return true;
    }

    public int getExpectedResultsCount()
    {
        return -1; /* no hints */
    }
}
