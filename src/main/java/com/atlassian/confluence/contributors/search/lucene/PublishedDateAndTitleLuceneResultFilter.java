package com.atlassian.confluence.contributors.search.lucene;

import com.atlassian.confluence.search.v2.lucene.LuceneResultFilter;
import com.atlassian.confluence.search.v2.lucene.WrappedHit;
import com.atlassian.confluence.search.v2.ResultFilter;

import java.util.ArrayList;
import java.util.List;

public class PublishedDateAndTitleLuceneResultFilter implements LuceneResultFilter
{
    private LuceneResultFilter contentTitleLuceneResultFilter;

    private LuceneResultFilter blogPostPublishedDateLuceneResultFilter;

    public PublishedDateAndTitleLuceneResultFilter(
            final LuceneResultFilter contentTitleLuceneResultFilter,
            final LuceneResultFilter blogPostPublishedDateLuceneResultFilter)
    {
        this.contentTitleLuceneResultFilter = contentTitleLuceneResultFilter;
        this.blogPostPublishedDateLuceneResultFilter = blogPostPublishedDateLuceneResultFilter;
    }

    public boolean isIncludedInResults(WrappedHit wrappedHit)
    {
        return contentTitleLuceneResultFilter.isIncludedInResults(wrappedHit)
                && blogPostPublishedDateLuceneResultFilter.isIncludedInResults(wrappedHit);
    }

    public boolean shouldContinueIterating()
    {
        return true;
    }

    public int getExpectedResultsCount()
    {
        return -1; /* no hint */
    }
}
