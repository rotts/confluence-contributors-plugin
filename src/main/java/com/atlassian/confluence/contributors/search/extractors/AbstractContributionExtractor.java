package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.plugins.index.api.Extractor2;

public abstract class AbstractContributionExtractor implements Extractor2
{
    public static final String CONTRIBUTION_TOKEN_SEPARATOR = "<>";

    @Override
    public StringBuilder extractText(final Object o)
    {
        return null;
    }
}
