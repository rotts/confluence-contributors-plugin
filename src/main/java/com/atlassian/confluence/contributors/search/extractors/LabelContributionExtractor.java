package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.pages.AbstractPage;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.SystemUtils;

import java.util.Collection;

public class LabelContributionExtractor extends AbstractContributionExtractor
{
    public static final String LABEL_CONTRIBUTIONS = "labelContributions";

    @Override
    public Collection<FieldDescriptor> extractFields(final Object searchable)
    {
        if (searchable instanceof AbstractPage)
        {
            final AbstractPage abstractPage = (AbstractPage) searchable;
            final StringBuilder labelContributionBuffer = new StringBuilder();

            for (final Labelling labelling : abstractPage.getLabellings())
            {
                final Label label = labelling.getLabel();

                labelContributionBuffer
                        .append(StringEscapeUtils.escapeHtml(Namespace.PERSONAL.equals(label.getNamespace()) ? label.toStringWithOwnerPrefix() : label.toStringWithNamespace()))
                        .append(CONTRIBUTION_TOKEN_SEPARATOR)
                        .append(label.getLastModificationDate().getTime())
                        .append(CONTRIBUTION_TOKEN_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml(labelling.getOwningUser().getKey().getStringValue()))
                        .append(SystemUtils.LINE_SEPARATOR);
            }

            return ImmutableList.of(new FieldDescriptor(LABEL_CONTRIBUTIONS, labelContributionBuffer.toString(),
                    FieldDescriptor.Store.YES, FieldDescriptor.Index.NOT_ANALYZED));
        }

        return null;
    }
}
