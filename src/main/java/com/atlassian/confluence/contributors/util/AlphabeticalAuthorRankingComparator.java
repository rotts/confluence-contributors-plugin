package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

/**
 * @author Brock Janiczak
 */
public class AlphabeticalAuthorRankingComparator implements Comparator {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Object obj1, Object obj2) {
		AuthorRanking ranking1 = (AuthorRanking) obj1;
		AuthorRanking ranking2 = (AuthorRanking) obj2;
		
		// finally order alphabetically
		String lhsFullName = ranking1.getFullNameString();
		String rhsFullName = ranking2.getFullNameString();
		
		if(lhsFullName != null && rhsFullName != null){
			return ranking1.getFullNameString().compareToIgnoreCase(ranking2.getFullNameString());
		}else if(lhsFullName == null && rhsFullName == null){
				// both have null names 
				return 0;
		}else if(lhsFullName == null){
			return -1;
		}else{
			return 1;
		}
		
	}

}
