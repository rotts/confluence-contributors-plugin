package com.atlassian.confluence.contributors.util;

import com.atlassian.confluence.pages.AbstractPage;

import java.util.List;

/**
 * Utility class to gather page details
 *
 * @author shannon
 */
public interface PageDetailsHelper
{
    static final String ANONYMOUS_USER = "Anonymous";

    static final int GROUPBY_CONTRIBUTORS = 2;

    static final int GROUPBY_PAGES = 4;

    /**
     * Process a list of pages into input author ranking system. Results can
     * either be grouped by contributors or pages.
     *
     * @param pageList      The list of AbstractPages to process.
     * @param rankingSystem The ranking system to store the results in
     * @param groupBy       How the results will be group, either by contributor or by
     *                      page. Refer to class static fields for valid input values.
     */
    void processPageList(List<AbstractPage> pageList, AuthorRankingSystem rankingSystem, int groupBy);

    /**
	 * Process a list of pages into input author ranking system. Results can
	 * either be grouped by contributors or pages. When grouping by contributors
	 * what fields are included can be regulated by the boolean parameters.
	 *
	 * @param pages
	 *            The list of AbstractPages to process.
	 * @param rankingSystem
	 *            The ranking system to store the results in
	 * @param groupBy
	 *            How the results will be group, either by contributor or by
	 *            page. Refer to class static fields for valid input values.
	 * @param contributors
	 *            Include edits (not currently implemented for groupby pages)
	 * @param comments
	 *            Include comments (not currently implemented for groupby pages)
	 * @param labels
	 *            Include labels (not currently implemented for groupby pages)
	 */
    void processPageList(
            final List<AbstractPage> pages,
            final AuthorRankingSystem rankingSystem,
            final int groupBy,
            final boolean contributors,
            final boolean comments,
            final boolean labels,
            final boolean watches);
}