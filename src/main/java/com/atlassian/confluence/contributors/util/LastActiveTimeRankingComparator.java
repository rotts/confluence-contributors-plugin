package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

/**
 * @author Shannon Krebs
 */
public class LastActiveTimeRankingComparator implements Comparator {

	public int compare(Object obj1, Object obj2) {
		AuthorRanking ranking1 = (AuthorRanking)obj1;
		AuthorRanking ranking2 = (AuthorRanking)obj2;
		
		
		if (ranking1.getLastActiveTime() < ranking2.getLastActiveTime()) {
			return 1;
		}
		if (ranking1.getLastActiveTime() > ranking2.getLastActiveTime()) {
			return -1;
		}
		
		// finally order alphabetically
		return new AlphabeticalAuthorRankingComparator().compare(ranking1, ranking2);
		
	}

}
