package com.atlassian.confluence.contributors.util;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;


public class AuthorRanking {
	
	private int edits;
	private int comments;
	private int labels;
	private int watches;
	private long lastEditTime;
	private long lastCommentTime;
	private long lastLabelTime;
	private TreeSet labelList;

	
	private TreeMap labelMap;
	private TreeMap editMap;
	private TreeMap commentMap;
	private TreeMap watchMap;
	
	private String idString;
	private String fullNameString;
   
	
	public AuthorRanking(String authorId, String fullName, long lastEditTime){
		this.idString = authorId;
		this.fullNameString = fullName;
		this.lastEditTime = lastEditTime;
		
		this.lastCommentTime = 0;
		this.lastLabelTime = 0;
		labelList = new TreeSet();

		
		labelMap = new TreeMap();
		editMap = new TreeMap();
		commentMap = new TreeMap();
		watchMap = new TreeMap();

	}
	
	public AuthorRanking(String authorId, String fullName){
		
		this(authorId, fullName, 0L);
	}
	
	public String getIdString() {
		return this.idString;
	}
	
	public String getFullNameString() {
		return this.fullNameString;
	}
	
	/**
	 * Returns the last edit time for this author ranking.
	 * @return lastEditTime for this AuthorRanking
	 */
	public long getLastEditTime() {
		return lastEditTime;
	}
	
	public void setLastEditTime(long lastEditTime) {
		this.lastEditTime = lastEditTime;
	}
	
	public void setLastCommentTime(long lastCommentTime) {
		this.lastCommentTime = lastCommentTime;
	}

	/**
	 * The author edited the content of the page.  
	 */
	public AuthorRanking incrementEdits() {
		edits++;
		return this;
	}
	
	public AuthorRanking incrementComments(){
		comments++;
		return this;
	}
	
	public AuthorRanking incrementLabels(){
		labels++;
		return this;
	}
	
	
	public void incrementWatches(String id, String desc){
		
		// We only want to count a watch if it is unique
		if(!watchMap.containsKey(id)){
			watches++;
			watchMap.put(id, desc);
		}
	}
	
	
	public void incrementEdits(String id, String desc, long editTime) {
		edits++;
		editMap.put(id,desc);
		if(editTime > this.lastEditTime){
			this.lastEditTime = editTime;
		}
		
	}
	
	public void incrementComments(String id, String desc, long commentTime){
		comments++;
		commentMap.put(id, desc);
		if(commentTime > this.lastCommentTime){
			this.lastCommentTime = commentTime;
		}
		
	}
	
	public void incrementLabels(String id, String desc, long labelTime) {
		labels++;
		labelMap.put(id, desc);
		if (labelTime > this.lastLabelTime) {
			this.lastLabelTime = labelTime;
		}

	}

	
	
	
	
	public int getEdits() {
		return edits;
	}
	
	public String toString(){
		return idString + " " + fullNameString + " " + new Date(lastEditTime);
	}

	public int getComments() {
		return comments;
	}

	public int getLabels() {
		return labels;
	}

	public long getLastCommentTime() {
		return lastCommentTime;
	}

	public long getLastLabelTime() {
		return lastLabelTime;
	}

	public void setLastLabelTime(long lastLabelTime) {
		this.lastLabelTime = lastLabelTime;
	}
	
	/**
	 * Returns the latest date from all the date fields
	 * @return latest value from lastEditTime, lastCommentTime and lastLabelTime
	 */
	public long getLastActiveTime(){
		long ret = lastEditTime;
		if(ret < lastLabelTime){
			ret = lastLabelTime;
		}
		if(ret < lastCommentTime){
			ret = lastCommentTime;
		}
		return ret;
		
	}
	
	public Date getLastActiveDate(){
		return new Date(getLastActiveTime());
	}
	

	public Set getLabelList() {
		return labelList;
	}
	
	public String getLabelListString(){
		StringBuffer ret = new StringBuffer();
		Iterator itr = labelList.iterator();
		while(itr.hasNext()){
			ret.append((String)itr.next());
			ret.append(" ");
		}
		return ret.toString();
		
	}

	public void addLabel(String label){
		labelList.add(label);
	}

	public void setEdits(int edits) {
		this.edits = edits;
	}
	
	public int getTotalCount(){
		return comments + edits + labels + watches;
	}

	public TreeMap getCommentMap() {
		return commentMap;
	}

	public TreeMap getEditMap() {
		return editMap;
	}

	public TreeMap getLabelMap() {
		return labelMap;
	}

	public int getWatches() {
		return watches;
	}

	public void setWatches(int watches) {
		this.watches = watches;
	}

	public TreeMap getWatchMap() {
		return watchMap;
	}

	public void setWatchMap(TreeMap watchMap) {
		this.watchMap = watchMap;
	}

		

	
}