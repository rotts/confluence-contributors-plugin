package com.atlassian.confluence.contributors.util;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.bonnie.ILuceneConnection;
import com.atlassian.bonnie.LuceneConnection;
import com.atlassian.bonnie.LuceneUtils;
import com.atlassian.confluence.contributors.search.extractors.AbstractContributionExtractor;
import com.atlassian.confluence.contributors.search.extractors.AuthorContributionExtractor;
import com.atlassian.confluence.contributors.search.extractors.CommentContributionExtractor;
import com.atlassian.confluence.contributors.search.extractors.LabelContributionExtractor;
import com.atlassian.confluence.contributors.search.extractors.NotificationContributionExtractor;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.User;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.base.Strings;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.TermQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

///CLOVER:OFF
// Due to usages of the LuceneConnection.SearcherAction, this class is hard to unit test. Best leave this
// for integration tests.
public class DefaultPageDetailsHelper implements PageDetailsHelper
{
    private static final Logger logger = LoggerFactory.getLogger(PageDetailsHelper.class);

    private static final String GLOBAL_LABEL_PREFIX = Namespace.GLOBAL.getPrefix() + ":";

    private final ILuceneConnection luceneConnection;

    private final UserAccessor userAccessor;

    public DefaultPageDetailsHelper(UserAccessor userAccessor)
    {
        luceneConnection = ComponentLocator.getComponent(ILuceneConnection.class, "luceneConnection");
        this.userAccessor = userAccessor;
    }

    public void processPageList(List<AbstractPage> pageList, AuthorRankingSystem rankingSystem, int groupBy)
    {
		processPageList(pageList, rankingSystem, groupBy, true, true, true, true);
	}

	public void processPageList(
            final List<AbstractPage> pages,
            final AuthorRankingSystem rankingSystem,
			final int groupBy,
            final boolean contributors,
            final boolean comments,
            final boolean labels,
            final boolean watches)
    {
        final String name = "PageDetailsHelper::processPageList(List<AbstractPage, AuthorRankingSystem, int, boolean, boolean, boolean, boolean):void";

        UtilTimerStack.push(name);

        try
        {
            switch (groupBy)
            {
            case GROUPBY_CONTRIBUTORS:
                for (final AbstractPage page : pages)
                    processAbstractPageContributors(page, rankingSystem, contributors, comments, labels, watches);
                break;
            case GROUPBY_PAGES:
                for (final AbstractPage page : pages)
                    processAbstractPage(page, rankingSystem);
                break;
            }
        }
        finally
        {
            UtilTimerStack.pop(name);
        }
    }


	/**
	 * Private helper method to populate the ranking system grouped by contributors.
	 */
	public void processAbstractPageContributors(
            final AbstractPage page,
            final AuthorRankingSystem rankingSystem,
            final boolean authors,
            final boolean comments,
            final boolean labels,
            final boolean watches)
    {
		if (authors)
			processAuthors(page, rankingSystem, GROUPBY_CONTRIBUTORS);

        if (comments)
			processComments(page, rankingSystem, GROUPBY_CONTRIBUTORS);

        if (labels)
			processLabels(page, rankingSystem, GROUPBY_CONTRIBUTORS);

        if (watches)
			processWatches(page, rankingSystem, GROUPBY_CONTRIBUTORS);
	}

	/**
	 * Private helper method to populate the ranking system grouped by pages.
	 * @param page
	 * @param rankingSystem
	 */
	private void processAbstractPage(
            final AbstractPage page,
            final AuthorRankingSystem rankingSystem) {
        processAuthors(page, rankingSystem, GROUPBY_PAGES);
        processComments(page, rankingSystem, GROUPBY_PAGES);
        processLabels(page, rankingSystem, GROUPBY_PAGES);
        processWatches(page, rankingSystem, GROUPBY_PAGES);
	}

    /**
	 * Private helper method to add the authors of all previous version of this
	 * page to the supplied AuthorRankingSystem.
	 */
	private void processAuthors(final AbstractPage abstractPage, final AuthorRankingSystem rankingSystem, final int groupingType)
    {
        luceneConnection.withSearch(new LuceneConnection.SearcherAction()
        {
            public void perform(final IndexSearcher indexSearcher) throws IOException
            {
                indexSearcher.search(
                        new TermQuery(new Term("handle", new HibernateHandle(abstractPage).toString())),
                        new BaseCollector()
                        {
                            @Override
                            public void collect(final int doc) throws IOException
                            {
                                BufferedReader reader = null;
                                try
                                {
                                    final Document document = indexSearcher.doc(docBase + doc);
                                    String line;

                                    reader = new BufferedReader(new StringReader(StringUtils.defaultString(document.get(AuthorContributionExtractor.AUTHOR_CONTRIBUTIONS))));
                                    while (null != (line = reader.readLine()))
                                    {
                                        final String[] authorContributionTokens = StringUtils.splitByWholeSeparator(line, AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR);

                                        if (authorContributionTokens.length == 2 && !ArrayUtils.contains(authorContributionTokens, StringUtils.EMPTY))
                                        {
                                            final long lastModifiedTime = Long.parseLong(authorContributionTokens[1]);
                                            final User author = userAccessor.getUserByKey(new UserKey(StringEscapeUtils.unescapeHtml(authorContributionTokens[0])));
                                            final String authorName = null == author ? ANONYMOUS_USER : author.getName();
                                            final String authorFullName = null == author ? ANONYMOUS_USER : author.getFullName();

                                            AuthorRanking ranking;

                                            if (GROUPBY_CONTRIBUTORS == groupingType)
                                            {
                                                ranking = rankingSystem.getAuthorRanking(authorName);

                                                if (ranking == null)
                                                    ranking = rankingSystem.createAuthorRanking(authorName, authorFullName, lastModifiedTime);

                                                ranking.incrementEdits(abstractPage.getUrlPath(), abstractPage.getTitle(), lastModifiedTime);
                                            }
                                            else if (GROUPBY_PAGES == groupingType)
                                            {
                                                final String pageUrlPath = abstractPage.getUrlPath();
                                                ranking = rankingSystem.getAuthorRanking(pageUrlPath);

                                                if (ranking == null)
                                                    ranking = rankingSystem.createAuthorRanking(pageUrlPath, abstractPage.getTitle());

                                                ranking.incrementEdits(authorName, authorFullName, lastModifiedTime);
                                            }
                                        }
                                    }
                                }
                                catch (final IOException ioe)
                                {
                                    logger.error("Error collecting information to calculate edit contributions of " + abstractPage, ioe);
                                }
                                finally
                                {
                                    IOUtils.closeQuietly(reader);
                                }

                            }
                        }
                );
            }
        });
	}

	/**
	 * Private helper method to add all the details of the comments on a page to
	 * the supplied AuthorRankingSystem
	 */
	private void processComments(
            final AbstractPage abstractPage,
            final AuthorRankingSystem rankingSystem,
            final int groupingType)
    {
        luceneConnection.withSearch(new LuceneConnection.SearcherAction()
        {
            public void perform(final IndexSearcher indexSearcher) throws IOException
            {
                indexSearcher.search(
                        new TermQuery(new Term(CommentContributionExtractor.CONTAINING_PAGE_ID, abstractPage.getIdAsString())),
                        new BaseCollector()
                        {
                            @Override
                            public void collect(final int doc) throws IOException
                            {
                                try
                                {
                                    final Document document = indexSearcher.doc(docBase + doc);
                                    final long lastModifiedTime = LuceneUtils.stringToDate(document.get("modified")).getTime();

                                    String authorName = ANONYMOUS_USER;
                                    String authorFullName = ANONYMOUS_USER;
                                    String lastModifierName = document.get("lastModifierName");
                                    if (!Strings.isNullOrEmpty(lastModifierName))
                                    {
                                        final User author = userAccessor.getUserByKey(new UserKey(lastModifierName));
                                        authorName = author.getName();
                                        authorFullName = author.getFullName();
                                    }

                                    AuthorRanking ranking;

                                    if (GROUPBY_CONTRIBUTORS == groupingType)
                                    {
                                        ranking = rankingSystem.getAuthorRanking(authorName);

                                        if (ranking == null)
                                            ranking = rankingSystem.createAuthorRanking(authorName, authorFullName);

                                        ranking.incrementComments(abstractPage.getUrlPath(), abstractPage.getTitle(), lastModifiedTime);
                                    }
                                    else if (GROUPBY_PAGES == groupingType)
                                    {
                                        final String pageUrlPath = abstractPage.getUrlPath();
                                        ranking = rankingSystem.getAuthorRanking(pageUrlPath);

                                        if (ranking == null)
                                            ranking = rankingSystem.createAuthorRanking(pageUrlPath, abstractPage.getTitle());

                                        ranking.incrementComments(authorName, authorFullName, lastModifiedTime);
                                    }
                                }
                                catch (final IOException ioe)
                                {
                                    logger.error("Error collecting information to calculate comment contributions of " + abstractPage, ioe);
                                }

                            }
                        }
                );
            }
        });
	}

	/**
	 * Private helper method to add the labels from a page to the supplied
	 * AuthorRankingSystem
	 */
	private void processLabels(
            final AbstractPage abstractPage,
            final AuthorRankingSystem rankingSystem,
            final int groupingType)
    {
        luceneConnection.withSearch(new LuceneConnection.SearcherAction()
        {
            public void perform(final IndexSearcher indexSearcher) throws IOException
            {
                indexSearcher.search(
                        new TermQuery(new Term("handle", new HibernateHandle(abstractPage).toString())),
                        new BaseCollector()
                        {
                            @Override
                            public void collect(final int doc) throws IOException
                            {
                                BufferedReader reader = null;
                                try
                                {
                                    final Document document = indexSearcher.doc(docBase + doc);
                                    String line;

                                    reader = new BufferedReader(new StringReader(StringUtils.defaultString(document.get(LabelContributionExtractor.LABEL_CONTRIBUTIONS))));
                                    while (null != (line = reader.readLine()))
                                    {
                                        final String[] labelContributionTokens = StringUtils.splitByWholeSeparator(line, AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR);

                                        if (labelContributionTokens.length == 3 && !ArrayUtils.contains(labelContributionTokens, StringUtils.EMPTY))
                                        {
                                            final String label = StringEscapeUtils.unescapeHtml(labelContributionTokens[0]);
                                            final long lastModifiedTime = Long.parseLong(labelContributionTokens[1]);

                                            String authorName = ANONYMOUS_USER;
                                            String authorFullName = ANONYMOUS_USER;
                                            String contributor = labelContributionTokens[2];
                                            if (!Strings.isNullOrEmpty(contributor))
                                            {
                                                final User author = userAccessor.getUserByKey(new UserKey(StringEscapeUtils.unescapeHtml(contributor)));
                                                authorName = author.getName();
                                                authorFullName = author.getFullName();
                                            }

                                            AuthorRanking ranking;

                                            if (GROUPBY_CONTRIBUTORS == groupingType)
                                            {
                                                ranking = rankingSystem.getAuthorRanking(authorName);

                                                if (ranking == null)
                                                    ranking = rankingSystem.createAuthorRanking(authorName, authorFullName);

                                                ranking.incrementLabels(abstractPage.getUrlPath(), abstractPage.getTitle(), lastModifiedTime);
                                                ranking.addLabel(LabelParser.parse(label.startsWith(GLOBAL_LABEL_PREFIX) ? label.substring(GLOBAL_LABEL_PREFIX.length()) : label).toLabel().toString());
                                            }
                                            else if (GROUPBY_PAGES == groupingType)
                                            {
                                                final String pageUrlPath = abstractPage.getUrlPath();
                                                ranking = rankingSystem.getAuthorRanking(pageUrlPath);

                                                if (ranking == null)
                                                    ranking = rankingSystem.createAuthorRanking(pageUrlPath, abstractPage.getTitle());

                                                ranking.incrementLabels(authorName, authorFullName, lastModifiedTime);
                                                ranking.addLabel(LabelParser.parse(label.startsWith(GLOBAL_LABEL_PREFIX) ? label.substring(GLOBAL_LABEL_PREFIX.length()) : label).toLabel().toString());
                                            }
                                        }
                                    }
                                }
                                catch (final IOException ioe)
                                {
                                    logger.error("Error collecting information to calculate label contributions of " + abstractPage, ioe);
                                }
                                finally
                                {
                                    IOUtils.closeQuietly(reader);
                                }
                            }
                        }
                );
            }
        });
	}

	/**
	 * Private helper method to add page watches to the supplied
	 * AuthorRankingSystem
	 */
	private void processWatches(
            final AbstractPage abstractPage,
            final AuthorRankingSystem rankingSystem,
            final int groupingType) {
        luceneConnection.withSearch(new LuceneConnection.SearcherAction()
        {
            public void perform(final IndexSearcher indexSearcher) throws IOException
            {
                final BooleanQuery booleanQuery = new BooleanQuery();

                booleanQuery.add(new TermQuery(new Term("handle", new HibernateHandle(abstractPage).toString())), BooleanClause.Occur.SHOULD);
                booleanQuery.add(new TermQuery(new Term("handle", new HibernateHandle(abstractPage.getSpace()).toString())), BooleanClause.Occur.SHOULD);


                indexSearcher.search(
                        booleanQuery,
                        new BaseCollector()
                        {
                            @Override
                            public void collect(final int doc) throws IOException
                            {
                                BufferedReader reader = null;
                                try
                                {
                                    final Document document = indexSearcher.doc(docBase + doc);
                                    String line;

                                    reader = new BufferedReader(new StringReader(StringUtils.defaultString(document.get(NotificationContributionExtractor.WATCHERS))));
                                    while (null != (line = reader.readLine()))
                                    {
                                        if (StringUtils.isNotBlank(line))
                                        {
                                            final User watcher = userAccessor.getUserByKey(new UserKey(line));
                                            final String authorName = null == watcher ? ANONYMOUS_USER : watcher.getName();
                                            final String authorFullName = null == watcher ? ANONYMOUS_USER : watcher.getFullName();
                                            AuthorRanking ranking;

                                            if (GROUPBY_CONTRIBUTORS == groupingType)
                                            {
                                                ranking = rankingSystem.getAuthorRanking(authorName);

                                                if (ranking == null)
                                                    ranking = rankingSystem.createAuthorRanking(authorName, authorFullName);

                                                ranking.incrementWatches(abstractPage.getUrlPath(), abstractPage.getTitle());
                                            }
                                            else if (GROUPBY_PAGES == groupingType)
                                            {
                                                final String pageUrlPath = abstractPage.getUrlPath();
                                                ranking = rankingSystem.getAuthorRanking(pageUrlPath);

                                                if (ranking == null)
                                                    ranking = rankingSystem.createAuthorRanking(pageUrlPath, abstractPage.getTitle());

                                                ranking.incrementWatches(authorName, authorFullName);
                                            }
                                        }
                                    }
                                }
                                catch (final IOException ioe)
                                {
                                    logger.error("Error collecting information to calculate label contributions of " + abstractPage, ioe);
                                }
                                finally
                                {
                                    IOUtils.closeQuietly(reader);
                                }
                            }
                        }
                );
            }
        });
    }

    private static abstract class BaseCollector extends Collector
    {
        protected int docBase;

        @Override
        public void setScorer(final Scorer scorer) throws IOException
        {
        }

        @Override
        public void setNextReader(final AtomicReaderContext context) throws IOException
        {
            docBase = context.docBase;
        }

        @Override
        public boolean acceptsDocsOutOfOrder()
        {
            return true;
        }
    }
}
///CLOVER:ON