package com.atlassian.confluence.contributors.util;

import com.atlassian.confluence.pages.AbstractPage;

import java.util.Calendar;
import java.util.List;

public interface PageSearchHelper
{
    List<AbstractPage> pageList(String pageName, String spaceKey, String labels, String scope, Calendar publishDate, String contentType);
}