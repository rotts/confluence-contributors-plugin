package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

/**
 * @author Shannon Krebs
 */
public class EditTimeAuthorRankingComparator implements Comparator {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Object obj1, Object obj2) {
		AuthorRanking ranking1 = (AuthorRanking)obj1;
		AuthorRanking ranking2 = (AuthorRanking)obj2;
		
		
		if (ranking1.getLastEditTime() < ranking2.getLastEditTime()) {
			return 1;
		}
		if (ranking1.getLastEditTime() > ranking2.getLastEditTime()) {
			return -1;
		}
		
		// finally order alphabetically
		return new AlphabeticalAuthorRankingComparator().compare(ranking1, ranking2);
		
		

		
	}

}