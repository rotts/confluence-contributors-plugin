package com.atlassian.confluence.contributors.util;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.contributors.search.filter.PublishedDateAndTitleResultFilter;
import com.atlassian.confluence.core.ListBuilder;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.BooleanOperator;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.PermittedSpacesQuery;
import com.atlassian.confluence.search.v2.query.TextFieldQuery;
import com.atlassian.confluence.search.v2.searchfilter.ContentPermissionsSearchFilter;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

///CLOVER:OFF
// This class need not to be instrumented. Useful validations can only be done with integration tests.
public class DefaultPageSearchHelper implements PageSearchHelper
{
    private static final Logger log = LoggerFactory.getLogger(PageSearchHelper.class);

    public static final String SCOPE_CHILDREN = "children";

    public static final String SCOPE_ANCESTORS = "ancestors";

    public static final String SCOPE_DESCENDANTS = "descendants";

    public static final String SPACES_ALL = "@all";

    public static final String SPACES_GLOBAL = "@global";

    public static final String SPACES_PERSONAL = "@personal";

    public static final String CONTENT_TYPE_PAGES = "pages";

    public static final String CONTENT_TYPE_BLOGPOSTS = "blogposts";

    private final SpaceManager spaceManager;

    private final SearchManager searchManager;

    private final LabelManager labelManager;

    public DefaultPageSearchHelper(SpaceManager spaceManager, SearchManager searchManager, LabelManager labelManager)
    {
        this.spaceManager = spaceManager;
        this.searchManager = searchManager;
        this.labelManager = labelManager;
    }

    private Set<String> getSpaceKeys(final ListBuilder<Space> spaces)
    {
        List<Space> spaceList = spaces.getPage(0, spaces.getAvailableSize());
        return getSpaceKeys(spaceList);
    }

    private Set<String> getSpaceKeys(final List<Space> spaces)
    {
        final Set<String> spaceKeys = new HashSet<String>(spaces.size());

        for (Space space : spaces)
            spaceKeys.add(space.getKey());

        return spaceKeys;
    }

    private SearchQuery getSpacesQuery(final String spaceKey)
    {
        final Set<String> spaceKeys;

        if (StringUtils.equals(SPACES_ALL, spaceKey))
            spaceKeys = getSpaceKeys(spaceManager.getAllSpaces());
        else if (StringUtils.equals(SPACES_GLOBAL, spaceKey))
            spaceKeys = getSpaceKeys(spaceManager.getSpaces(SpacesQuery.newQuery().withSpaceType(SpaceType.GLOBAL).build()));
        else if (StringUtils.equals(SPACES_PERSONAL, spaceKey))
            spaceKeys = getSpaceKeys(spaceManager.getSpaces(SpacesQuery.newQuery().withSpaceType(SpaceType.PERSONAL).build()));
        else
        {
            spaceKeys = new HashSet<String>(Arrays.asList(StringUtils.split(spaceKey, " ,")));
        }

        return new InSpaceQuery(spaceKeys);
    }

    private SearchQuery getContentQuery(
            final String contentTitle,
            final String contentType,
            final String labelsString,
            final Calendar publishedDate)
    {
        if (StringUtils.isBlank(contentTitle)
                && StringUtils.isBlank(labelsString)
                && null == publishedDate)
        {
            final Set<SearchQuery> contentTypeQueries = new HashSet<SearchQuery>(2);

            contentTypeQueries.add(new ContentTypeQuery(Arrays.asList(ContentTypeEnum.BLOG, ContentTypeEnum.PAGE)));

            return BooleanQuery.composeOrQuery(contentTypeQueries);
        }
        else
        {
            final Set<SearchQuery> queries = new HashSet<SearchQuery>();

            /* If contentType is not specified, query all blog posts and pages. Otherwise, query either one of them. */
            queries.add(
                    StringUtils.isBlank(contentType)
                            ? new ContentTypeQuery(Arrays.asList(ContentTypeEnum.BLOG, ContentTypeEnum.PAGE))
                            : (new ContentTypeQuery(StringUtils.equals(Page.CONTENT_TYPE, contentType) ? ContentTypeEnum.PAGE : ContentTypeEnum.BLOG)));

            if (StringUtils.isNotBlank(labelsString))
            {
                final String[] labels = StringUtils.split(labelsString, " ,");
                final Set<SearchQuery> labelQueries = new HashSet<SearchQuery>(labels.length);

                for (final String labelName : StringUtils.split(labelsString, " ,"))
                    labelQueries.add(new TextFieldQuery("labelText", QueryParser.escape(labelName), BooleanOperator.AND));

                queries.add(BooleanQuery.composeOrQuery(labelQueries));
            }

            return BooleanQuery.composeAndQuery(queries);
        }
    }

    private Set<AbstractPage> getMatchingContent(
            final SearchQuery searchQuery,
            final String contentTitle,
            final Calendar publishedDate,
            final String scope)
    {
        final String name = "PageSearchHelper::getMatchingContent(SearchQuery, String, Calendar, String):Set<AbstractPage>";

        UtilTimerStack.push(name);

        try
        {
            List<Searchable> searchResults = performSearch(searchQuery, contentTitle, publishedDate);
            return getSearchResultAsContentEntityObjects(scope, searchResults);
        }
        catch (final InvalidSearchException ise)
        {
                log.error("Unable to search for content.", ise);

            return Collections.emptySet();
        }
        finally
        {
            UtilTimerStack.pop(name);
        }
    }

    private Set<AbstractPage> getSearchResultAsContentEntityObjects(
            final String scope,
            final List<Searchable> searchables)
    {
        final String name = "PageSearchHelper::getSearchResultAsContentEntityObjects(String, List<Searchable> searchable):Set<AbstractPage>";


        UtilTimerStack.push(name);

        try
        {
            final Set<AbstractPage> contents = new HashSet<AbstractPage>(searchables.size());

            for (final Searchable searchResult : searchables)
            {
                if (searchResult instanceof AbstractPage)
                {
                    final AbstractPage ceo = (AbstractPage) searchResult;

                    contents.add(ceo);

                    if (StringUtils.equals(Page.CONTENT_TYPE, ceo.getType()))
                    {
                        final Page page = (Page) ceo;

                        if (StringUtils.equals(SCOPE_CHILDREN, scope))
                            contents.addAll(page.getChildren());
                        else if (StringUtils.equals(SCOPE_DESCENDANTS, scope))
                            contents.addAll(page.getDescendents());
                        else if (StringUtils.equals(SCOPE_ANCESTORS, scope))
                            contents.addAll(page.getAncestors());
                    }
                }
            }

            return contents;
        }
        finally
        {
            UtilTimerStack.pop(name);
        }
    }

    private List<Searchable> performSearch(SearchQuery searchQuery, String contentTitle, Calendar publishedDate)
            throws InvalidSearchException
    {
        final String name = "PageSearchHelper::performSearch(SearchQuery, String, Calendar):SearchResult";

        UtilTimerStack.push(name);

        try
        {
            return searchManager.searchEntities(
                    new ContentSearch(
                            searchQuery, null, ContentPermissionsSearchFilter.getInstance(), new PublishedDateAndTitleResultFilter(contentTitle, publishedDate, Calendar.DAY_OF_MONTH)
                    )
            );

        }
        finally
        {
            UtilTimerStack.pop(name);
        }
    }

    public List<AbstractPage> pageList(String pageName, String spaceKey, String labels, String scope, Calendar publishDate, String contentType)
    {
        final String name = "PageSearchHelper::pageList(String, String, String, String, Calendar, String):List<ContentEntityObject>";

        UtilTimerStack.push(name);

        try
        {
            // We need at least a spaceKey to do anything
            if (StringUtils.isBlank(spaceKey))
                throw new IllegalArgumentException("Space key cannot be blank/null.");

            List<SearchQuery> query = new ArrayList<SearchQuery>();

            query.add(getSpacesQuery(spaceKey));
            query.add(getContentQuery(pageName, contentType, labels, publishDate));
            query.add(PermittedSpacesQuery.getInstance());

            final Collection<Label> matchLabels = Collections2.filter(
                    Collections2.transform(
                            Collections2.filter(
                                    Collections2.transform(
                                            Arrays.asList(StringUtils.split(StringUtils.defaultString(labels), ", ")),
                                            new Function<String, ParsedLabelName>()
                                            {
                                                @Override
                                                public ParsedLabelName apply(String labelName)
                                                {
                                                    return LabelParser.parse(labelName);
                                                }
                                            }
                                    ),
                                    Predicates.<Object>notNull()
                            ),
                            new Function<ParsedLabelName, Label>()
                            {
                                @Override
                                public Label apply(ParsedLabelName parsedLabelName)
                                {
                                    return labelManager.getLabel(parsedLabelName);
                                }
                            }
                    ),
                    Predicates.<Object>notNull()
            );

            return new ArrayList<AbstractPage>(
                    Collections2.filter(
                            getMatchingContent(BooleanQuery.composeAndQuery(new HashSet<SearchQuery>(query)), pageName, publishDate, scope),
                            new Predicate<AbstractPage>()
                            {
                                @Override
                                public boolean apply(AbstractPage abstractPage)
                                {
                                    if (matchLabels.isEmpty())
                                        return true;

                                    for (Label matchingLabel : matchLabels)
                                    {
                                        Collection<Label> pageLabels = abstractPage.getLabels();
                                        if (pageLabels.contains(matchingLabel))
                                            return true;
                                    }

                                    return false;
                                }
                            }
                    )
            );
        }
        finally
        {
            UtilTimerStack.pop(name);
        }
    }
}
///CLOVER:ON