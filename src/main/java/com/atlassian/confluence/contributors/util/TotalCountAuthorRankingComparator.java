package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

/**
 * @author Shannon Krebs
 */
public class TotalCountAuthorRankingComparator implements Comparator {

	public int compare(Object obj1, Object obj2) {
		AuthorRanking ranking1 = (AuthorRanking)obj1;
		AuthorRanking ranking2 = (AuthorRanking)obj2;
		
		int total1 = ranking1.getTotalCount();
		int total2 = ranking2.getTotalCount();
		
		if (total1 < total2) {
			return 1;
		}
		
		if (total1 > total2) {
			return -1;
		}
		
		// finally order alphabetically
		return new AlphabeticalAuthorRankingComparator().compare(ranking1, ranking2);
	}

}
