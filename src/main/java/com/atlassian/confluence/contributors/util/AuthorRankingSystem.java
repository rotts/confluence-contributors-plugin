package com.atlassian.confluence.contributors.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class AuthorRankingSystem {
	
	private Map authorRankMap = new HashMap();
	private final RankType rankingType;
	
	public AuthorRankingSystem(RankType rankingType) {
		this.rankingType = rankingType;
	}
	
	public boolean hasRanking(String authorId) {
		return authorRankMap.containsKey(authorId);
	}
	
	public AuthorRanking createAuthorRanking(String authorId, String fullName, long lastEditTime) {
		
		AuthorRanking ranking = new AuthorRanking(authorId, fullName, lastEditTime);
		authorRankMap.put(authorId, ranking);
		
		return ranking;
	}
	
	public AuthorRanking createAuthorRanking(String authorId, String fullName){
		AuthorRanking ranking = new AuthorRanking(authorId, fullName);
		authorRankMap.put(authorId, ranking);
		
		return ranking;
	}
	
	public AuthorRanking getAuthorRanking(String authorId) {
		AuthorRanking ranking = (AuthorRanking) authorRankMap.get(authorId);
		return ranking;
	}
	
	/**
	 * Returns a sorted list of authors based on the Ranktype for this author
	 * ranking system. This method is equivalent to calling
	 * 
	 * <pre>
	 * getRankedAuthors(false)
	 * </pre>
	 * 
	 * @return sorted list of authors using the current ranktype
	 */
	public List getRankedAuthors() {
		return getRankedAuthors(false);
	}

	/**
	 * Returns a sorted list of authors based on the RankType for this author
	 * ranking system. If the reverse parameter is true the ordering for the
	 * current comparator will be reversed
	 * 
	 * @param reverse
	 *            true will reverse the ordering of the current comparator
	 * @return sorted list of authors using the current RankType.
	 */
	public List getRankedAuthors(boolean reverse){
		
		List authorRankings = new ArrayList(authorRankMap.values());
		final Comparator originalComparator = rankingType.getComparator();
        Comparator comparator;
        if(reverse){
            comparator = new Comparator()
            {
                public int compare(Object object, Object object1)
                {
                    return -(originalComparator.compare(object, object1));
                }
            };
		}
        else
        {
            comparator = originalComparator;
        }

        Collections.sort(authorRankings, comparator);
		
		return authorRankings;
		
	}
	
	/**
	 * Returns a list of sorted authors that only has the authors who have added
	 * content as specified by the input parameters.
	 * 
	 * @param reverse
	 *            true will reverse the ordering of the current comparator
	 * @param keepEditors
	 *            true and the return list will only contain contributors that
	 *            have an edit count greater then zero or included by another
	 *            parameter.
	 * @param keepCommentors
	 *            true and the return list will only contain authors that have a
	 *            comment count greater then zero or included by another
	 *            parameter.
	 * @param keepLabelers
	 *            true and the return list will only contain contributors that
	 *            have a label count greater then zero or included by another
	 *            parameter.
	 * @param keepWatches
	 *            true and the return list will only contain contributors that
	 *            have a watches count greater then zero or included by another
	 *            parameter.
	 * @return sorted list of contributors held by this author ranking system
	 *         using the current RankType.
	 */
	public List getRankedAuthors(boolean reverse, boolean keepEditors, boolean keepCommentors, boolean keepLabelers, boolean keepWatches){
		
		List sortedList = getRankedAuthors(reverse);
		
		
		for (Iterator iterator = sortedList.iterator(); iterator.hasNext();) {
			AuthorRanking authorRanking = (AuthorRanking) iterator.next();
			
			if( ! ((keepEditors && authorRanking.getEdits() > 0) 
				|| (keepCommentors && authorRanking.getComments() > 0)	
				|| (keepLabelers && authorRanking.getLabels() > 0) 
				|| (keepWatches && authorRanking.getWatches() > 0)))
			{
					iterator.remove();
			}
		}
		
		return sortedList;
		
	}
	
	/**
	 * Returns an unsorted list of AuthorRankings
	 * @return an unsorted list of author Rankings
	 */
	public List getAuthors(){
		return new ArrayList(authorRankMap.values());
	}
	
	public int size(){
		return authorRankMap.size();
	}
	
	public static class RankType {
		
		public static final RankType FULL_NAME = new RankType(new AlphabeticalAuthorRankingComparator());
		public static final RankType EDIT_COUNT = new RankType(new EditCountAuthorRankingComparator());
		public static final RankType TOTAL_COUNT = new RankType(new TotalCountAuthorRankingComparator());
		public static final RankType EDIT_TIME = new RankType(new EditTimeAuthorRankingComparator());
		public static final RankType LASTACTIVE_TIME = new RankType(new LastActiveTimeRankingComparator());

		
		private final Comparator comparator;
		
		private RankType(Comparator comparator) {
			this.comparator = comparator;
		}
		
		public Comparator getComparator() {
			return this.comparator;
		}
		
		
	}
}