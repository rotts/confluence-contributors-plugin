package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.core.persistence.ContentEntityObjectDao;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.impl.DefaultUser;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.lucene.document.Document;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;


public class TestAuthorContributionExtractor extends MockObjectTestCase
{
    private Mock mockContentEntityObjectDao;

    private ContentEntityObjectDao contentEntityObjectDao;

    private AuthorContributionExtractor authorContributionExtractor;

    private long lastModificationDateOfDocumentToBeIndex;

    private Page pageToBeIndexed;

    private BlogPost blogToBeIndexed;

    private ConfluenceUser admin;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockContentEntityObjectDao = new Mock(ContentEntityObjectDao.class);
        contentEntityObjectDao = (ContentEntityObjectDao) mockContentEntityObjectDao.proxy();

        authorContributionExtractor = new AuthorContributionExtractor();
        authorContributionExtractor.setContentEntityObjectDao(contentEntityObjectDao);

        lastModificationDateOfDocumentToBeIndex = System.currentTimeMillis();

        pageToBeIndexed = new Page();
        pageToBeIndexed.setId(1L);
        admin = newUser("admin");
        pageToBeIndexed.setLastModifier(admin);
        pageToBeIndexed.setLastModificationDate(new Date(lastModificationDateOfDocumentToBeIndex));

        blogToBeIndexed = new BlogPost();
        blogToBeIndexed.setId(1L);
        blogToBeIndexed.setLastModifier(admin);
        blogToBeIndexed.setLastModificationDate(new Date(lastModificationDateOfDocumentToBeIndex));
    }

    public void testFieldAddedWithPreviousVersionDataOfPage()
    {
        final List<VersionHistorySummary> versionHistorySummaries = new ArrayList<VersionHistorySummary>();
        final Calendar now = Calendar.getInstance();

        /* Create 3 previous versions */
        for (int i = 2; i >= 0; --i)
        {
            final VersionHistorySummary versionHistorySummary = new VersionHistorySummary(
                    pageToBeIndexed.getId() + i,
                    i,
                    newUser("<" + String.valueOf(i) + ">"), /* Test if HTML unsafe characters are properly escaped, it can be properly split by AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR */
                    now.getTime(),
                    StringUtils.EMPTY
            );

            versionHistorySummaries.add(versionHistorySummary);
            now.add(Calendar.DAY_OF_YEAR, -1);
        }

        mockContentEntityObjectDao.expects(once()).method("getVersionHistorySummary").with(eq(pageToBeIndexed.getId())).will(
                returnValue(versionHistorySummaries)
        );

        Collection<FieldDescriptor> fieldDescriptors = authorContributionExtractor.extractFields(pageToBeIndexed);
        FieldDescriptor authorContributionsField = fieldDescriptors.iterator().next();

        assertEquals(
                new StringBuffer()
                        .append(StringEscapeUtils.escapeHtml("<2>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(0).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml("<1>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(1).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml("<0>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(2).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .toString()
                , authorContributionsField.getValue());
    }

    public void testFieldAddedWithPreviousVersionDataOfBlogPost()
    {
        final List<VersionHistorySummary> versionHistorySummaries = new ArrayList<VersionHistorySummary>();
        final Calendar now = Calendar.getInstance();

        /* Create 3 previous versions */
        for (int i = 2; i >= 0; --i)
        {
            final VersionHistorySummary versionHistorySummary = new VersionHistorySummary(
                    blogToBeIndexed.getId() + i,
                    i,
                    newUser("<" + String.valueOf(i) + ">"), /* Test if HTML unsafe characters are properly escaped, it can be properly split by AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR */
                    now.getTime(),
                    StringUtils.EMPTY
            );

            versionHistorySummaries.add(versionHistorySummary);
            now.add(Calendar.DAY_OF_YEAR, -1);
        }

        mockContentEntityObjectDao.expects(once()).method("getVersionHistorySummary").with(eq(blogToBeIndexed.getId())).will(
                returnValue(versionHistorySummaries)
        );

        Collection<FieldDescriptor> fieldDescriptors = authorContributionExtractor.extractFields(blogToBeIndexed);
        FieldDescriptor authorContributionsField = fieldDescriptors.iterator().next();

        assertEquals(
                new StringBuffer()
                        .append(StringEscapeUtils.escapeHtml("<2>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(0).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml("<1>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(1).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml("<0>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(2).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .toString()
                , authorContributionsField.getValue());
    }

    public void testAddedFieldEmptyWhenThereIsNoPreviousVersion()
    {
        final List<VersionHistorySummary> versionHistorySummaries = new ArrayList<VersionHistorySummary>();

        mockContentEntityObjectDao.expects(once()).method("getVersionHistorySummary").with(eq(pageToBeIndexed.getId())).will(
                returnValue(versionHistorySummaries)
        );

        Collection<FieldDescriptor> fieldDescriptors = authorContributionExtractor.extractFields(pageToBeIndexed);
        FieldDescriptor authorContributionsField = fieldDescriptors.iterator().next();

        assertEquals(StringUtils.EMPTY, authorContributionsField.getValue());
    }

    public void testFieldNotAddedWhenSearchableIsNotAnAbstractPage()
    {
        Collection<FieldDescriptor> fieldDescriptors = authorContributionExtractor.extractFields(new Comment());

        assertNull(fieldDescriptors);
    }

    public void testVersionHistoryWithNullLastModificationDateNotIndexed()
    {
        final List<VersionHistorySummary> versionHistorySummaries = new ArrayList<VersionHistorySummary>();
        final Calendar now = Calendar.getInstance();
        final Document documentToBeIndexed;

        /* Create 3 previous versions */
        for (int i = 2; i >= 0; --i)
        {
            final VersionHistorySummary versionHistorySummary = new VersionHistorySummary(
                    blogToBeIndexed.getId() + i,
                    i,
                    newUser("<" + String.valueOf(i) + ">"), /* Test if HTML unsafe characters are properly escaped, it can be properly split by AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR */
                    (i == 2 || i == 1) ? now.getTime() : null,
                    StringUtils.EMPTY
            );

            versionHistorySummaries.add(versionHistorySummary);
            now.add(Calendar.DAY_OF_YEAR, -1);
        }
        

        mockContentEntityObjectDao.expects(once()).method("getVersionHistorySummary").with(eq(blogToBeIndexed.getId())).will(
                returnValue(versionHistorySummaries)
        );

        Collection<FieldDescriptor> fieldDescriptors = authorContributionExtractor.extractFields(blogToBeIndexed);
        FieldDescriptor authorContributionsField = fieldDescriptors.iterator().next();

        assertEquals(
                new StringBuffer()
                        .append(StringEscapeUtils.escapeHtml("<2>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(0).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml("<1>")).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR).append(versionHistorySummaries.get(1).getLastModificationDate().getTime()).append(SystemUtils.LINE_SEPARATOR)
                        .toString()
                , authorContributionsField.getValue());
    }

    private static ConfluenceUser newUser(String name)
    {
        return new ConfluenceUserImpl(new DefaultUser(name))
        {
            @Override
            public UserKey getKey()
            {
                return new UserKey(this.getName());
            }
        };
    }
}
