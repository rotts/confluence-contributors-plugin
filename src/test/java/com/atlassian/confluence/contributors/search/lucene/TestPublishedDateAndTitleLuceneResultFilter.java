package com.atlassian.confluence.contributors.search.lucene;

import com.atlassian.confluence.search.v2.lucene.LuceneResultFilter;
import com.atlassian.confluence.search.v2.lucene.WrappedHit;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

public class TestPublishedDateAndTitleLuceneResultFilter extends MockObjectTestCase
{
    private PublishedDateAndTitleLuceneResultFilter publishedDateAndTitleLuceneResultFilter;

    private Mock mockContentTitleLuceneResultFilter;

    private LuceneResultFilter contentTitleLuceneResultFilter;

    private Mock mockBlogPostPublishedDateLuceneResultFilter;

    private LuceneResultFilter blogPostPublishedDateLuceneResultFilter;

    private Mock mockWrappedHit;

    private WrappedHit wrappedHit;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockContentTitleLuceneResultFilter = new Mock(LuceneResultFilter.class);
        contentTitleLuceneResultFilter = (LuceneResultFilter) mockContentTitleLuceneResultFilter.proxy();

        mockBlogPostPublishedDateLuceneResultFilter = new Mock(LuceneResultFilter.class);
        blogPostPublishedDateLuceneResultFilter = (LuceneResultFilter) mockBlogPostPublishedDateLuceneResultFilter.proxy();

        publishedDateAndTitleLuceneResultFilter = new PublishedDateAndTitleLuceneResultFilter(
                contentTitleLuceneResultFilter,
                blogPostPublishedDateLuceneResultFilter
        );

        mockWrappedHit = new Mock(WrappedHit.class);
        wrappedHit = (WrappedHit) mockWrappedHit.proxy();
    }

    public void testIsIncludedInResultsWhenContentTitleFilterDropsWrappedResult()
    {
        mockContentTitleLuceneResultFilter.expects(once()).method("isIncludedInResults")
                .with(same(wrappedHit)).will(returnValue(false));

        assertFalse(publishedDateAndTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsIncludedInResultsWhenBlogPostPublishedDateFilterDropsWrappedResult()
    {
        mockContentTitleLuceneResultFilter.expects(once()).method("isIncludedInResults")
                .with(same(wrappedHit)).will(returnValue(true));
        mockBlogPostPublishedDateLuceneResultFilter.expects(once()).method("isIncludedInResults")
                .with(same(wrappedHit)).will(returnValue(false));

        assertFalse(publishedDateAndTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsIncludedInResultsWhenBothContentFiltersAcceptWrappedResult()
    {
        mockContentTitleLuceneResultFilter.expects(once()).method("isIncludedInResults")
                .with(same(wrappedHit)).will(returnValue(true));
        mockBlogPostPublishedDateLuceneResultFilter.expects(once()).method("isIncludedInResults")
                .with(same(wrappedHit)).will(returnValue(true));

        assertTrue(publishedDateAndTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testFilterIsAlwaysContinuous()
    {
        assertTrue(publishedDateAndTitleLuceneResultFilter.shouldContinueIterating());
    }

    public void testFilterHintOnExpectedSize()
    {
        assertEquals(-1, publishedDateAndTitleLuceneResultFilter.getExpectedResultsCount());
    }
}
