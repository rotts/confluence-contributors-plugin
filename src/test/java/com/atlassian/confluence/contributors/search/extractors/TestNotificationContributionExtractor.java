package com.atlassian.confluence.contributors.search.extractors;

import org.jmock.MockObjectTestCase;


public class TestNotificationContributionExtractor extends MockObjectTestCase
{
//    private static final String PAGE_NOTIFICATION_QUERY = "select notif.userName from Notification as notif where notif.page.id = :pageId ";
//
//    private static final String SPACE_NOTIFICATION_QUERY = "select notif.userName from Notification as notif where notif.space.id = :spaceId ";
//
//    private Mock mockSessionFactory;
//
//    private SessionFactory sessionFactory;
//
//    private Mock mockSession;
//
//    private Session session;
//
//    private NotificationContributionExtractor notificationContributionExtractor;
//
//    protected void setUp() throws Exception
//    {
//        super.setUp();
//
//        mockSession = new Mock(Session.class);
//        session = (Session) mockSession.proxy();
//
//        mockSessionFactory = new Mock(SessionFactory.class);
//        sessionFactory = (SessionFactory) mockSessionFactory.proxy();
//
//        mockSession.expects(once()).method("getSessionFactory").withNoArguments().will(returnValue(sessionFactory));
//        mockSession.expects(once()).method("flush").withNoArguments();
//        mockSessionFactory.expects(once()).method("openSession").withNoArguments().will(returnValue(session));
//
//        notificationContributionExtractor = new NotificationContributionExtractor();
//        notificationContributionExtractor.setSessionFactory(sessionFactory);
//    }
//
//    public void testFieldAddedWhenIndexingSpaceWithWatchers()
//    {
//        final Document documentToBeIndex = new Document();
//        final Space spaceToBeIndexed = new Space();
//        final List<Notification> notifications = new ArrayList<Notification>();
//        Notification spaceNotification;
//
//        spaceNotification = new Notification();
//        spaceNotification.setUserName("admin");
//        notifications.add(spaceNotification);
//
//        spaceNotification = new Notification();
//        spaceNotification.setUserName("admin2");
//        notifications.add(spaceNotification);
//
//        mockSession.expects(once()).method("find").with(
//                eq(SPACE_NOTIFICATION_QUERY),
//                eq(new Object[] { spaceToBeIndexed.getId() }),
//                eq(new Type[] { Hibernate.LONG })
//        ).will(returnValue(Arrays.asList("admin", "admin2")));
//
//        notificationContributionExtractor.addFields(
//                documentToBeIndex,
//                new StringBuffer(),
//                spaceToBeIndexed);
//
//        assertNotNull(documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//        assertEquals(
//                "admin" + SystemUtils.LINE_SEPARATOR +
//                "admin2" + SystemUtils.LINE_SEPARATOR,
//                documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//    }
//
//    public void testFieldAddedWhenIndexingSpaceWithoutWatchers()
//    {
//        final Document documentToBeIndex = new Document();
//        final Space spaceToBeIndexed = new Space();
//
//        mockSession.expects(once()).method("find").with(
//                eq(SPACE_NOTIFICATION_QUERY),
//                eq(new Object[] { spaceToBeIndexed.getId() }),
//                eq(new Type[] { Hibernate.LONG })
//        ).will(returnValue(Collections.EMPTY_LIST));
//
//        notificationContributionExtractor.addFields(
//                documentToBeIndex,
//                new StringBuffer(),
//                spaceToBeIndexed);
//
//        assertNotNull(documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//        assertEquals(StringUtils.EMPTY,
//                documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//    }
//
//    public void testFieldAddedWhenIndexingPageWithWatchers()
//    {
//        final Document documentToBeIndex = new Document();
//        final Page pageToBeIndexed = new Page();
//        final List<Notification> notifications = new ArrayList<Notification>();
//        Notification spaceNotification;
//
//        spaceNotification = new Notification();
//        spaceNotification.setUserName("admin");
//        notifications.add(spaceNotification);
//
//        spaceNotification = new Notification();
//        spaceNotification.setUserName("admin2");
//        notifications.add(spaceNotification);
//
//        mockSession.expects(once()).method("find").with(
//                eq(PAGE_NOTIFICATION_QUERY),
//                eq(new Object[] { pageToBeIndexed.getId() }),
//                eq(new Type[] { Hibernate.LONG })
//        ).will(returnValue(Arrays.asList("admin", "admin2")));
//
//        notificationContributionExtractor.addFields(
//                documentToBeIndex,
//                new StringBuffer(),
//                pageToBeIndexed);
//
//        assertNotNull(documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//        assertEquals(
//                "admin" + SystemUtils.LINE_SEPARATOR +
//                "admin2" + SystemUtils.LINE_SEPARATOR,
//                documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//    }
//
//    public void testFieldAddedWhenIndexingPageWithoutWatchers()
//    {
//        final Document documentToBeIndex = new Document();
//        final Page pageToBeIndexed = new Page();
//
//        mockSession.expects(once()).method("find").with(
//                eq(PAGE_NOTIFICATION_QUERY),
//                eq(new Object[] { pageToBeIndexed.getId() }),
//                eq(new Type[] { Hibernate.LONG })
//        ).will(returnValue(Collections.EMPTY_LIST));
//
//        notificationContributionExtractor.addFields(
//                documentToBeIndex,
//                new StringBuffer(),
//                pageToBeIndexed);
//
//        assertNotNull(documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//        assertEquals(StringUtils.EMPTY,
//                documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
//    }
//
    public void testFieldNotAddedWhenIndexingSearchableWhichIsNotSpaceOrAbstractPage()
    {
//        final Document documentToBeIndex = new Document();
//
//        mockSession.reset(); /* No expectations */
//        mockSessionFactory.reset(); /* No expectations */
//
//        notificationContributionExtractor.addFields(
//                documentToBeIndex,
//                new StringBuffer(),
//                new Comment());
//
//        assertNull(documentToBeIndex.get(NotificationContributionExtractor.WATCHERS));
    }
}
