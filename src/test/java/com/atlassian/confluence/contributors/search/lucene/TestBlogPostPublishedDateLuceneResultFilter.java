package com.atlassian.confluence.contributors.search.lucene;

import com.atlassian.bonnie.LuceneUtils;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.search.v2.lucene.WrappedHit;
import org.apache.lucene.index.CorruptIndexException;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.io.IOException;
import java.util.Calendar;

public class TestBlogPostPublishedDateLuceneResultFilter extends MockObjectTestCase
{
    private BlogPostPublishedDateLuceneResultFilter blogPostPublishedDateLuceneResultFilter;

    private Mock mockWrappedHit;

    private WrappedHit wrappedHit;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockWrappedHit = new Mock(WrappedHit.class);
        wrappedHit = (WrappedHit) mockWrappedHit.proxy();
    }

    public void testIsResultIncludedWhenPublishedDateToLookForIsNull()
    {
        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(null, Calendar.DAY_OF_MONTH);
        assertTrue(blogPostPublishedDateLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenTypeOfWrappedHitIsNotBlogPost()
    {
        mockWrappedHit.expects(once()).method("get").with(eq("type")).will(returnValue("page"));
        
        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(Calendar.getInstance(), Calendar.DAY_OF_MONTH);
        assertTrue(blogPostPublishedDateLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenDayOfMonthOfWrappedHitCreationDateDoesNotMatchDayOfMonthOfPublishedDateToLookFor()
    {
        final Calendar publishedDateToLookFor = Calendar.getInstance();
        final Calendar wrappedHitCreated = Calendar.getInstance();

        wrappedHitCreated.set(
                publishedDateToLookFor.get(Calendar.YEAR),
                publishedDateToLookFor.get(Calendar.MONTH),
                publishedDateToLookFor.get(Calendar.DAY_OF_MONTH) + 1);

        mockWrappedHit.expects(once()).method("get").with(eq("type")).will(returnValue(BlogPost.CONTENT_TYPE));
        mockWrappedHit.expects(once()).method("get").with(eq("created")).will(returnValue(LuceneUtils.dateToString(wrappedHitCreated.getTime())));

        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(publishedDateToLookFor, Calendar.DAY_OF_MONTH);
        assertFalse(blogPostPublishedDateLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenDayOfMonthOfWrappedHitCreationDateDoesMatchesDayOfMonthOfPublishedDateToLookFor()
    {
        final Calendar publishedDateToLookFor = Calendar.getInstance();

        mockWrappedHit.expects(once()).method("get").with(eq("type")).will(returnValue(BlogPost.CONTENT_TYPE));
        mockWrappedHit.expects(once()).method("get").with(eq("created")).will(returnValue(LuceneUtils.dateToString(publishedDateToLookFor.getTime())));

        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(publishedDateToLookFor, Calendar.DAY_OF_MONTH);
        assertTrue(blogPostPublishedDateLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenIndexIsCorrupt()
    {
        mockWrappedHit.expects(once()).method("get").with(eq("type")).will(throwException(new CorruptIndexException("fakeCorruptIndexException")));

        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(Calendar.getInstance(), Calendar.DAY_OF_MONTH);
        assertFalse(blogPostPublishedDateLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenIoErrorOccuredWhileReadingFromWrappedHit()
    {
        mockWrappedHit.expects(once()).method("get").with(eq("type")).will(throwException(new IOException("fakeIOException")));

        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(Calendar.getInstance(), Calendar.DAY_OF_MONTH);
        assertFalse(blogPostPublishedDateLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testFilterIsAlwaysContinuous()
    {
        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(null, Calendar.DAY_OF_MONTH);
        assertTrue(blogPostPublishedDateLuceneResultFilter.shouldContinueIterating());
    }

    public void testFilterHintOnExpectedSize()
    {
        blogPostPublishedDateLuceneResultFilter = new BlogPostPublishedDateLuceneResultFilter(null, Calendar.DAY_OF_MONTH);
        assertEquals(-1, blogPostPublishedDateLuceneResultFilter.getExpectedResultsCount());
    }
}
