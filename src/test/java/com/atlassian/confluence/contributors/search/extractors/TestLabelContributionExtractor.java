package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.impl.DefaultUser;

import junit.framework.TestCase;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

public class TestLabelContributionExtractor extends TestCase
{

    private LabelContributionExtractor labelContributionExtractor;

    private TestPage pageToBeIndexed;

    private TestBlog blogToBeIndexed;

    private String testSpaceKey;
    private ConfluenceUserImpl admin;

    protected void setUp() throws Exception
    {
        super.setUp();

        labelContributionExtractor = new LabelContributionExtractor();
        testSpaceKey = "tst";

        pageToBeIndexed = new TestPage();
        pageToBeIndexed.setId(1);

        blogToBeIndexed = new TestBlog();
        blogToBeIndexed.setId(2);
        admin = new ConfluenceUserImpl(new DefaultUser("admin"))
        {
            @Override
            public UserKey getKey()
            {
                return new UserKey("admin");
            }
        };
    }

    public void testFieldAddedWithLabelsOfPage()
    {
        final List<Labelling> labellings = new ArrayList<Labelling>();
        final Calendar now = Calendar.getInstance();

        Label label;
        Labelling labelling;

        label = new Label("label1", Namespace.PERSONAL)
        {
            public String getOwner()
            {
                return "admin";
            }
        };
        label.setId(1);
        label.setLastModificationDate(now.getTime());
        labelling = new Labelling(label, pageToBeIndexed, admin);
        labellings.add(labelling);
        now.add(Calendar.DAY_OF_YEAR, -1);

        label = new Label("label2", Namespace.TEAM);
        label.setId(2);
        label.setLastModificationDate(now.getTime());
        labelling = new Labelling(label, pageToBeIndexed, admin);
        labellings.add(labelling);
        now.add(Calendar.DAY_OF_YEAR, -1);

        label = new Label("label3", Namespace.GLOBAL);
        label.setId(3);
        label.setLastModificationDate(now.getTime());
        labelling = new Labelling(label, pageToBeIndexed, admin);
        labellings.add(labelling);

        pageToBeIndexed.setLabellings(labellings);


        Collection<FieldDescriptor> fieldDescriptors = labelContributionExtractor.extractFields(pageToBeIndexed);
        FieldDescriptor fieldDescriptor = fieldDescriptors.iterator().next();

        assertEquals(
                new StringBuffer()
                        .append(StringEscapeUtils.escapeHtml(labellings.get(0).getLabel().toStringWithOwnerPrefix())).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(0).getLabel().getLastModificationDate().getTime()).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(0).getUser()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml(labellings.get(1).getLabel().toStringWithNamespace())).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(1).getLabel().getLastModificationDate().getTime()).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(1).getUser()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml(labellings.get(2).getLabel().toStringWithNamespace())).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(2).getLabel().getLastModificationDate().getTime()).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(2).getUser()).append(SystemUtils.LINE_SEPARATOR)
                        .toString()
                , fieldDescriptor.getValue());
    }

    public void testFieldAddedWithLabelsOfBlogPost()
    {
        final List<Labelling> labellings = new ArrayList<Labelling>();
        final Calendar now = Calendar.getInstance();

        Label label;
        Labelling labelling;

        label = new Label("label1", Namespace.PERSONAL)
        {
            public String getOwner()
            {
                return "admin";
            }
        };
        label.setId(1);
        label.setLastModificationDate(now.getTime());
        labelling = new Labelling(label, blogToBeIndexed, admin);
        labellings.add(labelling);
        now.add(Calendar.DAY_OF_YEAR, -1);

        label = new Label("label2", Namespace.TEAM);
        label.setId(2);
        label.setLastModificationDate(now.getTime());
        labelling = new Labelling(label, blogToBeIndexed, admin);
        labellings.add(labelling);
        now.add(Calendar.DAY_OF_YEAR, -1);

        label = new Label("label3", Namespace.GLOBAL);
        label.setId(3);
        label.setLastModificationDate(now.getTime());
        labelling = new Labelling(label, blogToBeIndexed, admin);
        labellings.add(labelling);

        blogToBeIndexed.setLabellings(labellings);


        Collection<FieldDescriptor> fieldDescriptors = labelContributionExtractor.extractFields(blogToBeIndexed);
        FieldDescriptor fieldDescriptor = fieldDescriptors.iterator().next();

        assertEquals(
                new StringBuffer()
                        .append(StringEscapeUtils.escapeHtml(labellings.get(0).getLabel().toStringWithOwnerPrefix())).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(0).getLabel().getLastModificationDate().getTime()).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(0).getUser()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml(labellings.get(1).getLabel().toStringWithNamespace())).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(1).getLabel().getLastModificationDate().getTime()).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(1).getUser()).append(SystemUtils.LINE_SEPARATOR)
                        .append(StringEscapeUtils.escapeHtml(labellings.get(2).getLabel().toStringWithNamespace())).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(2).getLabel().getLastModificationDate().getTime()).append(AbstractContributionExtractor.CONTRIBUTION_TOKEN_SEPARATOR)
                                .append(labellings.get(2).getUser()).append(SystemUtils.LINE_SEPARATOR)
                        .toString()
                , fieldDescriptor.getValue());
    }

    public void testFieldNotEmptyWhenThereIsNoLabel()
    {
        final List<Labelling> labellings = new ArrayList<Labelling>();

        pageToBeIndexed.setLabellings(labellings);

        Collection<FieldDescriptor> fieldDescriptors = labelContributionExtractor.extractFields(pageToBeIndexed);
        FieldDescriptor fieldDescriptor = fieldDescriptors.iterator().next();

        assertEquals(
                StringUtils.EMPTY
                , fieldDescriptor.getValue());
    }

    public void testFieldNotAddedWhenSearchableIsNotAnAbstractPage()
    {
        final List<Labelling> labellings = new ArrayList<Labelling>();
        final SpaceDescription spaceDesc = new SpaceDescription()
        {
            public List getLabellings()
            {
                return labellings;
            }
        };

        Collection<FieldDescriptor> fieldDescriptors = labelContributionExtractor.extractFields(spaceDesc);

        assertNull(fieldDescriptors);
    }

    private static class TestPage extends Page
    {
        private List labellings;

        public List getLabellings()
        {
            return labellings;
        }

        public void setLabellings(List labellings)
        {
            this.labellings = labellings;
        }
    }

    private static class TestBlog extends BlogPost
    {
        private List labellings;

        public List getLabellings()
        {
            return labellings;
        }

        public void setLabellings(List labellings)
        {
            this.labellings = labellings;
        }
    }
}
