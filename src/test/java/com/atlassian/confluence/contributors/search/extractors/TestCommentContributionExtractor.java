package com.atlassian.confluence.contributors.search.extractors;

import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import junit.framework.TestCase;

import java.util.Collection;

public class TestCommentContributionExtractor extends TestCase
{
    private Page pageOfCommentToBeIndexed;

    private Comment commentToBeIndexed;

    private CommentContributionExtractor commentContributionExtractor;

    protected void setUp() throws Exception
    {
        super.setUp();

        pageOfCommentToBeIndexed = new Page();
        pageOfCommentToBeIndexed.setId(1L);

        commentToBeIndexed = new Comment();
        pageOfCommentToBeIndexed.addComment(commentToBeIndexed);

        commentContributionExtractor = new CommentContributionExtractor();
    }

    public void testFieldAddedWhenSearchableIsComment()
    {
        Collection<FieldDescriptor> fieldDescriptors = commentContributionExtractor.extractFields(commentToBeIndexed);

        assertEquals(pageOfCommentToBeIndexed.getId(), Long.parseLong(fieldDescriptors.iterator().next().getValue()));
    }

    public void testFieldNotAddedWhenSearchableIsNotComment()
    {
        Collection<FieldDescriptor> fieldDescriptors = commentContributionExtractor.extractFields(pageOfCommentToBeIndexed);

        assertNull(fieldDescriptors);
    }


    public void testFieldNotAddedWhenSearchableIsNotPageComment()
    {
        NonPageComment nonPageComment = new NonPageComment();

        Collection<FieldDescriptor> fieldDescriptors = commentContributionExtractor.extractFields(nonPageComment);

        assertNull(fieldDescriptors);
    }

    class NonPageComment extends Comment
    {
        @Override
        public AbstractPage getPage()
        {
            throw new IllegalStateException("Comment is not owned by a Page");
        }
    }
}
