package com.atlassian.confluence.contributors.search.lucene;

import com.atlassian.confluence.search.v2.lucene.WrappedHit;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.apache.lucene.index.CorruptIndexException;

import java.io.IOException;

public class TestContentTitleLuceneResultFilter extends MockObjectTestCase
{
    private ContentTitleLuceneResultFilter contentTitleLuceneResultFilter;

    private Mock mockWrappedHit;

    private WrappedHit wrappedHit;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockWrappedHit = new Mock(WrappedHit.class);
        wrappedHit = (WrappedHit) mockWrappedHit.proxy();
    }

    public void testIsResultIncludedWhenContentTitleToSearchForIsNull()
    {
        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter(null);
        assertTrue(contentTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenContentTitleToSearchForBlank()
    {
        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter("   ");
        assertTrue(contentTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenContentTitleToSearchForDoesNotMatchTitleReturnedByWrappedHit()
    {
        mockWrappedHit.expects(once()).method("get").with(eq("title")).will(returnValue("nonMatchingTitleReturnedByWrappedHit"));

        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter("titleToLookFor");
        assertFalse(contentTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenContentTitleToSearchForMatchesTitleReturnedByWrappedHit()
    {
        mockWrappedHit.expects(once()).method("get").with(eq("title")).will(returnValue("titleToLookFor"));

        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter("titleToLookFor");
        assertTrue(contentTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenIndexIsCorrupt()
    {
        mockWrappedHit.expects(once()).method("get").with(eq("title")).will(throwException(new CorruptIndexException("fakeCorruptIndexException")));

        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter("titleToLookFor");
        assertFalse(contentTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testIsResultIncludedWhenIoErrorOccuredWhileReadingFromWrappedHit()
    {
        mockWrappedHit.expects(once()).method("get").with(eq("title")).will(throwException(new IOException("fakeIOException")));

        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter("titleToLookFor");
        assertFalse(contentTitleLuceneResultFilter.isIncludedInResults(wrappedHit));
    }

    public void testFilterIsAlwaysContinuous()
    {
        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter("titleToLookFor");
        assertTrue(contentTitleLuceneResultFilter.shouldContinueIterating());
    }

    public void testFilterHintOnExpectedSize()
    {
        contentTitleLuceneResultFilter = new ContentTitleLuceneResultFilter("titleToLookFor");
        assertEquals(-1, contentTitleLuceneResultFilter.getExpectedResultsCount());
    }
}
