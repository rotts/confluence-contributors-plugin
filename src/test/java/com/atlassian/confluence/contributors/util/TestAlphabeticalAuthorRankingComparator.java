package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

public class TestAlphabeticalAuthorRankingComparator extends AbstractAuthorRankingComparatorTest {

    private Comparator comparator;

    public Comparator getComparator() {
        if (null == comparator)
            comparator = new AlphabeticalAuthorRankingComparator();

        return comparator;
    }

    public void testLesser() {
        final AuthorRanking lhs = createAuthorRanking("jane.doe", "Jane Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe");

        assertFalse(getComparator().compare(lhs, rhs) > 0);
        assertFalse(getComparator().compare(lhs, rhs) == 0);
        assertTrue(getComparator().compare(lhs, rhs) < 0);
    }

    public void testEquals() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "JoHn Doe");


        assertFalse(getComparator().compare(lhs, rhs) < 0);
        assertFalse(getComparator().compare(lhs, rhs) > 0);
        assertTrue(getComparator().compare(lhs, rhs) == 0);

    }
    
    public void testGreater() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "Jane Doe");

        assertFalse(getComparator().compare(lhs, rhs) < 0);
        assertFalse(getComparator().compare(lhs, rhs) == 0);
        assertTrue(getComparator().compare(lhs, rhs) > 0);
    }
    
    public void testNullFullNames(){
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "JoHn Doe");
        final AuthorRanking nullName = createAuthorRanking(null, null);
        
        assertTrue(getComparator().compare(lhs, nullName) > 0);
        assertTrue(getComparator().compare(nullName, rhs) < 0);
        assertTrue(getComparator().compare(nullName, nullName) == 0);
        
    }
}