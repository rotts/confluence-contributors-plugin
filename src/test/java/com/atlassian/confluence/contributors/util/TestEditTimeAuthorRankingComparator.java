package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

public class TestEditTimeAuthorRankingComparator extends AbstractAuthorRankingComparatorTest {

    private Comparator comparator;

    public Comparator getComparator() {
        if (null == comparator)
            comparator = new EditTimeAuthorRankingComparator();

        return comparator;
    }

    public void testLeftHandSideLesserOnLastEdit() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe");

        lhs.setLastEditTime(Long.MIN_VALUE);
        rhs.setLastEditTime(Long.MAX_VALUE);
        
        /*
         * Older edits mean more. The older your edit is, the closer you are to the tail of the ordered collection
         */
        assertTrue(getComparator().compare(lhs, rhs) > 0);
    }

    public void testLeftHandSideLesserOnCaseInsensitiveFullName() {
        final AuthorRanking lhs = createAuthorRanking("jane.doe", "JaNe Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe");

        lhs.setLastEditTime(Long.MAX_VALUE);
        rhs.setLastEditTime(Long.MAX_VALUE);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(rhs.getFullNameString()));
    }

    public void testEquals() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "john doe");

        lhs.setLastEditTime(Long.MIN_VALUE);
        rhs.setLastEditTime(Long.MAX_VALUE);

        assertFalse(0 == getComparator().compare(lhs, rhs));

        lhs.setLastEditTime(Long.MAX_VALUE);
        rhs.setLastEditTime(Long.MIN_VALUE);

        assertFalse(0 == getComparator().compare(lhs, rhs));

        lhs.setLastEditTime(Long.MAX_VALUE);
        rhs.setLastEditTime(Long.MAX_VALUE);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(rhs.getFullNameString()));
    }
    
    /**
	 * If the users have the same edit time a user with no name should print
	 * before one with a name. This situation shouldn't normally happen in
	 * Confluence since the full name is required, but errors have been reported
	 * in some cases where the comparator is throwing a null pointer exception
	 * in this area.
	 */
    public void testWithNullFullName(){
    	
    	final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "john doe");
    	final AuthorRanking nullName = createAuthorRanking(null, null);
        

        lhs.setLastEditTime(Long.MAX_VALUE);
        rhs.setLastEditTime(Long.MAX_VALUE);
        nullName.setLastEditTime(Long.MIN_VALUE);

        assertTrue(getComparator().compare(lhs, nullName) < 0);
        assertTrue(getComparator().compare(nullName, rhs) > 0);

        lhs.setLastEditTime(Long.MIN_VALUE);
        rhs.setLastEditTime(Long.MIN_VALUE);
        nullName.setLastEditTime(Long.MAX_VALUE);

        assertTrue(getComparator().compare(lhs, nullName) > 0);
        assertTrue(getComparator().compare(nullName, rhs) < 0);

        lhs.setLastEditTime(Long.MAX_VALUE);
        rhs.setLastEditTime(Long.MAX_VALUE);

        assertTrue(getComparator().compare(lhs, nullName) > 0);
        assertTrue(getComparator().compare(nullName, rhs) < 0);
        
        
        assertTrue(getComparator().compare(nullName, nullName) == 0);
                    	
    	
    }

    public void testLeftHandSideGreaterOnLastEdit() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe");

        lhs.setLastEditTime(Long.MAX_VALUE);
        rhs.setLastEditTime(Long.MIN_VALUE);
        
        /*
         * Newer edits mean less. The newer your edit is, the closer you are to the head of the ordered collection
         */
        assertTrue(getComparator().compare(lhs, rhs) < 0);
    }

    public void testLeftHandSideGreaterOnCaseInsensitiveFullName() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe", System.currentTimeMillis());
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "Jane Doe", System.currentTimeMillis());

        lhs.setLastEditTime(Long.MAX_VALUE);
        rhs.setLastEditTime(Long.MAX_VALUE);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(rhs.getFullNameString()));
    }
}