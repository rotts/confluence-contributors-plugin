package com.atlassian.confluence.contributors.util;

import org.jmock.MockObjectTestCase;

public abstract class AbstractTest extends MockObjectTestCase {

    public AuthorRanking createAuthorRanking(
            final String userName,
            final String fullName) {
        return new AuthorRanking(
                userName,
                fullName);
    }

    public AuthorRanking createAuthorRanking(
            final String userName,
            final String fullName,
            final long lastEditTime) {
        return new AuthorRanking(
                userName,
                fullName,
                lastEditTime);
    }
    
}