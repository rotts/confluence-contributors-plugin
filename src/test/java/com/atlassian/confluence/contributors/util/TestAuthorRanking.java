package com.atlassian.confluence.contributors.util;

import java.util.Map;

public class TestAuthorRanking extends AbstractTest {

    private static final int INCREMENT_COUNT = 20;

    public void testLastActive() {
        final AuthorRanking lhs = new AuthorRanking("john.doe", "John Doe");
        final long lastEditTime = Short.MAX_VALUE;
        final long lastLabelTime = Integer.MAX_VALUE;
        final long lastCommentTime = Long.MAX_VALUE;

        lhs.setLastEditTime(lastEditTime);
        assertEquals(lastEditTime, lhs.getLastActiveTime());
        assertEquals(lastEditTime, lhs.getLastEditTime());

        lhs.setLastLabelTime(lastLabelTime);
        assertEquals(lastLabelTime, lhs.getLastActiveTime());
        assertEquals(lastEditTime, lhs.getLastEditTime());
        assertEquals(lastLabelTime, lhs.getLastLabelTime());

        lhs.setLastCommentTime(lastCommentTime);
        assertEquals(lastCommentTime, lhs.getLastActiveTime());
        assertEquals(lastEditTime, lhs.getLastEditTime());
        assertEquals(lastLabelTime, lhs.getLastLabelTime());
        assertEquals(lastCommentTime, lhs.getLastCommentTime());
    }

    public void testIncrementEdits() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");
        int targetEditCount = INCREMENT_COUNT;

        for (int i = 1; i <= targetEditCount; ++i)
            authorRanking.incrementEdits();

        assertEquals(targetEditCount, authorRanking.getEdits());
    }

    public void testIncrementEditsWithMoreData() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");
        int targetEditCount = INCREMENT_COUNT;

        assertEquals(0, authorRanking.getLastEditTime());

        for (int i = 1; i <= targetEditCount; ++i) {
            final Map editMap;
            final String editMapKey = String.valueOf(i);
            final String editMapValue = String.valueOf(-i);

            authorRanking.incrementEdits(editMapKey, editMapValue, i);

            editMap = authorRanking.getEditMap();

            assertEquals(editMap.size(), i);
            assertNotNull(editMap.get(editMapKey));
            assertEquals(editMap.get(editMapKey), editMapValue);

            assertEquals(i, authorRanking.getLastEditTime());
        }
    }

    public void testIncrementLabels() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");
        int targetLabelCount = INCREMENT_COUNT;

        for (int i = 1; i <= targetLabelCount; ++i)
            authorRanking.incrementLabels();

        assertEquals(targetLabelCount, authorRanking.getLabels());
    }

    public void testIncrementLabelsWithMoreData() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");
        int targetLabelCount = INCREMENT_COUNT;

        assertEquals(0, authorRanking.getLastLabelTime());

        for (int i = 1; i <= targetLabelCount; ++i) {
            final Map labelMap;
            final String labelMapKey = String.valueOf(i);
            final String labelMapValue = String.valueOf(-i);

            authorRanking.incrementLabels(labelMapKey, labelMapValue, i);

            labelMap = authorRanking.getLabelMap();

            assertEquals(labelMap.size(), i);
            assertNotNull(labelMap.get(labelMapKey));
            assertEquals(labelMap.get(labelMapKey), labelMapValue);

            assertEquals(i, authorRanking.getLastLabelTime());
        }
    }

    public void testIncrementComments() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");
        int targetCommentCount = INCREMENT_COUNT;

        for (int i = 1; i <= targetCommentCount; ++i)
            authorRanking.incrementComments();

        assertEquals(targetCommentCount, authorRanking.getComments());
    }

    public void testIncrementCommentsWithMoreData() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");
        int targetCommentCount = INCREMENT_COUNT;

        assertEquals(0, authorRanking.getLastCommentTime());

        for (int i = 1; i <= targetCommentCount; ++i) {
            final Map commentMap;
            final String commentMapKey = String.valueOf(i);
            final String commentMapValue = String.valueOf(-i);

            authorRanking.incrementComments(commentMapKey, commentMapValue, i);

            commentMap = authorRanking.getCommentMap();

            assertEquals(commentMap.size(), i);
            assertNotNull(commentMap.get(commentMapKey));
            assertEquals(commentMap.get(commentMapKey), commentMapValue);

            assertEquals(i, authorRanking.getLastCommentTime());
        }
    }

    public void testIncrementWatchesWithMoreData() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");
        int targetWatchCount = INCREMENT_COUNT;

        for (int i = 1; i <= targetWatchCount; ++i) {
            final Map watchMap;
            final String watchMapKey = String.valueOf(i);
            final String watchMapValue = String.valueOf(-i);

            authorRanking.incrementWatches(watchMapKey, watchMapValue);

            watchMap = authorRanking.getWatchMap();

            assertEquals(watchMap.size(), i);
            assertNotNull(watchMap.get(watchMapKey));
            assertEquals(watchMap.get(watchMapKey), watchMapValue);
        }
    }

    public void testGetTotalCount() {
        final AuthorRanking authorRanking = createAuthorRanking("john.doe", "John Doe");


        for (int i = 0; i < INCREMENT_COUNT; ++i) {
            authorRanking.incrementEdits();
            authorRanking.incrementLabels();
            authorRanking.incrementComments();
        }

        authorRanking.setWatches(INCREMENT_COUNT);

        assertEquals(
                INCREMENT_COUNT << 2,
                authorRanking.getTotalCount());
    }
}