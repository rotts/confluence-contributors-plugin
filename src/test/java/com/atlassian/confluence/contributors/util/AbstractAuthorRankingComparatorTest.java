package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

public abstract class AbstractAuthorRankingComparatorTest extends AbstractTest {

    public abstract Comparator getComparator();
}