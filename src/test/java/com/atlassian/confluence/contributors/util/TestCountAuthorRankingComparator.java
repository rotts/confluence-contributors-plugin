package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

public class TestCountAuthorRankingComparator extends AbstractAuthorRankingComparatorTest {

    private Comparator comparator;

    public Comparator getComparator() {
        if (null == comparator)
            comparator = new TotalCountAuthorRankingComparator();

        return comparator;
    }

    protected void setCounts(
            final AuthorRanking authorRanking,
            final int editCount,
            final int labelCount,
            final int commentCount,
            final int watchCount) {
        setEditCount(authorRanking, editCount);
        setLabelCount(authorRanking, labelCount);
        setCommentCount(authorRanking, commentCount);
        setWatchCount(authorRanking, watchCount);
    }

    protected void setEditCount(
            final AuthorRanking authorRanking,
            final int editCount) {
        for (int i = 0; i < editCount; ++i)
            authorRanking.incrementEdits();
    }

    protected void setLabelCount(
            final AuthorRanking authorRanking,
            final int labelCount) {
        for (int i = 0; i < labelCount; ++i)
            authorRanking.incrementLabels();
    }

    protected void setCommentCount(
            final AuthorRanking authorRanking,
            final int commentCount) {
        for (int i = 0; i < commentCount; ++i)
            authorRanking.incrementComments();
    }

    protected void setWatchCount(
            final AuthorRanking authorRanking,
            final int watchCount) {
        authorRanking.setWatches(watchCount);
    }

    public void testLeftHandSideLesserOnTotal() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe");

        setCounts(lhs, 0, 1, 2, 3);
        setCounts(rhs, 1, 2, 3, 4);

        /* Less count mean more - The lesser the count you have, the closer you are to the tail of the ordered
        collection */
        assertTrue(getComparator().compare(lhs, rhs) > 0);
    }

    public void testLeftHandSideLesserOnCaseInsensitiveFullName() {
        final AuthorRanking lhs = createAuthorRanking("jane.doe", "JaNe Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe");

        setCounts(lhs, 0, 0, 0, 0);
        setCounts(rhs, 0, 0, 0, 0);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(rhs.getFullNameString()));
    }

    public void testEquals() {
        AuthorRanking lhs;
        AuthorRanking rhs;

        lhs = createAuthorRanking("john.doe", "John Doe");
        rhs = createAuthorRanking("jane.doe", "john doe");
        setCounts(lhs, 0, 1, 2, 3);
        setCounts(rhs, 1, 2, 3, 4);

        assertFalse(0 == getComparator().compare(lhs, rhs));


        lhs = createAuthorRanking("john.doe", "John Doe");
        rhs = createAuthorRanking("jane.doe", "john doe");
        setCounts(lhs, 1, 2, 3, 4);
        setCounts(rhs, 0, 1, 2, 3);

        assertFalse(0 == getComparator().compare(lhs, rhs));

        lhs = createAuthorRanking("john.doe", "John Doe");
        rhs = createAuthorRanking("jane.doe", "john doe");
        setCounts(lhs, 0, 1, 2, 3);
        setCounts(rhs, 0, 1, 2, 3);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(rhs.getFullNameString()));
    }
    
    /**
	 * If the users have the same edit counts a user with no name should print
	 * before one with a name. This situation shouldn't normally happen in
	 * Confluence since the full name is required, but errors have been reported
	 * in some cases where the comparator is throwing a null pointer exception
	 * in this area.
	 */
    public void testWithNullFullName(){
    	
    	final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "john doe");
    	final AuthorRanking nullName = createAuthorRanking(null, null);


    	setCounts(lhs, 0, 1, 2, 3);
    	setCounts(rhs, 0, 1, 2, 3);
    	setCounts(nullName, 1, 2, 3, 4);

    	assertTrue(getComparator().compare(lhs, nullName) > 0);
    	assertTrue(getComparator().compare(nullName, rhs) < 0);

    	setCounts(lhs, 1, 2, 3, 4);
    	setCounts(rhs, 1, 2, 3, 4);
    	setCounts(nullName, 0, 1, 2, 3);

    	assertTrue(getComparator().compare(lhs, nullName) < 0);
    	assertTrue(getComparator().compare(nullName, rhs) > 0);

    	setCounts(lhs, 0, 1, 2, 3);
    	setCounts(rhs, 0, 1, 2, 3);

        assertTrue(getComparator().compare(lhs, nullName) < 0);
        assertTrue(getComparator().compare(nullName, rhs) > 0);
        
        
        assertTrue(getComparator().compare(nullName, nullName) == 0);
                    	
    	
    }
    

    public void testLeftHandSideGreaterOnLastEdit() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe");

        setCounts(lhs, 1, 2, 3, 4);
        setCounts(rhs, 0, 1, 2, 3);

        /*
         * More count mean less. The more count you have, the closer you are to the start of the ordered
         * collection.
         */
        assertTrue(getComparator().compare(lhs, rhs) < 0);
    }

    public void testLeftHandSideGreaterOnCaseInsensitiveFullName() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe", System.currentTimeMillis());
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "Jane Doe", System.currentTimeMillis());

        setCounts(lhs, 0, 0, 0, 0);
        setCounts(rhs, 0, 0, 0, 0);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(rhs.getFullNameString()));
    }
}