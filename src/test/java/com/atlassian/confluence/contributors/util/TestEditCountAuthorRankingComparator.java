package com.atlassian.confluence.contributors.util;

import java.util.Comparator;

public class TestEditCountAuthorRankingComparator extends AbstractAuthorRankingComparatorTest {

    private Comparator comparator;

    public Comparator getComparator() {
        if (null == comparator)
            comparator = new EditCountAuthorRankingComparator();

        return comparator;
    }

    public void testLeftHandSideLesserOnEdits() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe", 0L);
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe", 0L);

        lhs.setEdits(Short.MAX_VALUE);
        rhs.setEdits(Integer.MAX_VALUE);

        /* less edits mean more - The less edits you have, the closer you are to the tail ordered collection */
        assertTrue(getComparator().compare(lhs, rhs) > 0);
    }

    public void testLeftHandSideLesserOnCaseInsensitiveFullName() {
        final AuthorRanking lhs = createAuthorRanking("jane.doe", "Jane Doe", 0L);
        final AuthorRanking rhs = createAuthorRanking("john.doe", "John Doe", 0L);

        lhs.setEdits(Integer.MAX_VALUE);
        rhs.setEdits(Integer.MAX_VALUE);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(
                        rhs.getFullNameString()));
    }

    public void testEquals() {
        final AuthorRanking lhs = createAuthorRanking("jane.doe", "John Doe", 0L);
        final AuthorRanking rhs = createAuthorRanking("john.doe", "john doe", 0L);

        lhs.setEdits(Short.MAX_VALUE);
        rhs.setEdits(Integer.MAX_VALUE);

        assertFalse(0 == getComparator().compare(lhs, rhs));

        lhs.setEdits(Integer.MAX_VALUE);
        rhs.setEdits(Short.MAX_VALUE);

        assertFalse(0 == getComparator().compare(lhs, rhs));

        lhs.setEdits(Integer.MAX_VALUE);
        rhs.setEdits(Integer.MAX_VALUE);
        
        assertEquals(0, getComparator().compare(lhs, rhs));
    }
    
    /**
	 * If the users have the same edit counts a user with no name should print
	 * before one with a name. This situation shouldn't normally happen in
	 * Confluence since the full name is required, but errors have been reported
	 * in some cases where the comparator is throwing a null pointer exception
	 * in this area.
	 */
    public void testWithNullFullName(){
    	
    	final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe");
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "john doe");
    	final AuthorRanking nullName = createAuthorRanking(null, null);
        

        lhs.setEdits(Integer.MAX_VALUE);
        rhs.setEdits(Integer.MAX_VALUE);
        nullName.setEdits(Short.MAX_VALUE);

        assertTrue(getComparator().compare(lhs, nullName) < 0);
        assertTrue(getComparator().compare(nullName, rhs) > 0);

        lhs.setEdits(Short.MAX_VALUE);
        rhs.setEdits(Short.MAX_VALUE);
        nullName.setEdits(Integer.MAX_VALUE);

        assertTrue(getComparator().compare(lhs, nullName) > 0);
        assertTrue(getComparator().compare(nullName, rhs) < 0);

        lhs.setEdits(Integer.MAX_VALUE);
        rhs.setEdits(Integer.MAX_VALUE);

        assertTrue(getComparator().compare(lhs, nullName) > 0);
        assertTrue(getComparator().compare(nullName, rhs) < 0);
        
        
        assertTrue(getComparator().compare(nullName, nullName) == 0);
                    	
    	
    }
    
    
    

    public void testLeftHandSideGreaterOnEdits() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe", 0L);
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "Jane Doe", 0L);

        lhs.setEdits(Integer.MAX_VALUE);
        rhs.setEdits(Short.MAX_VALUE);

        /* more edits mean less - The more edits you have, the closer you are to the head ordered collection */
        assertTrue(getComparator().compare(lhs, rhs) < 0);
    }

    public void testLeftHandSideGreaterOnCaseInsensitiveFullName() {
        final AuthorRanking lhs = createAuthorRanking("john.doe", "John Doe", 0L);
        final AuthorRanking rhs = createAuthorRanking("jane.doe", "Jane Doe", 0L);

        lhs.setEdits(Integer.MAX_VALUE);
        rhs.setEdits(Integer.MAX_VALUE);

        assertEquals(
                getComparator().compare(lhs, rhs),
                lhs.getFullNameString().compareToIgnoreCase(
                        rhs.getFullNameString()));
    }
}