package com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.contributors.util.AbstractTest;
import com.atlassian.confluence.contributors.util.PageDetailsHelper;
import com.atlassian.confluence.contributors.util.PageSearchHelper;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;
import org.jmock.Mock;
import org.jmock.core.Constraint;

import java.util.Collections;
import java.util.HashMap;

public class TestContributorsMacro extends AbstractTest
{
    private Mock mockLocaleManager;

    private Mock mockI18nBeanFactory;

    private Mock mockI18NBean;

    private Mock mockPageSearchHelper;

    private Mock mockPageDetailsHelper;

    private Mock mockVelocityHelperService;

    private Page content;

    private ContributorsMacro macro;

    public void setUp() throws Exception
    {
        super.setUp();

        macro = new ContributorsMacro(
                (LocaleManager) (mockLocaleManager = mock(LocaleManager.class)).proxy(),
                (I18NBeanFactory) (mockI18nBeanFactory = mock(I18NBeanFactory.class)).proxy(),
                (PageSearchHelper) (mockPageSearchHelper = mock(PageSearchHelper.class)).proxy(),
                (PageDetailsHelper) (mockPageDetailsHelper = mock(PageDetailsHelper.class)).proxy(),
                (VelocityHelperService) (mockVelocityHelperService = mock(VelocityHelperService.class)).proxy()
        );

        content = new Page();
        content.setSpace(new Space("DS"));
        content.setTitle("Test Page");

        mockPageSearchHelper.expects(once()).method("pageList").with(
                new Constraint[] { ANYTHING, ANYTHING, ANYTHING, ANYTHING, ANYTHING, ANYTHING }
        ).will(returnValue(Collections.<AbstractPage>emptyList()));
        mockPageDetailsHelper.expects(once()).method("processPageList").with(
                new Constraint[]{ANYTHING, ANYTHING, ANYTHING, ANYTHING, ANYTHING, ANYTHING, ANYTHING}
        );
    }

    public void testExecuteDefault()
    {
        final String renderText = macro.execute(
                new HashMap<String, String>()
                {
                    {
                        put("showPages", Boolean.TRUE.toString());
                    }
                },
                StringUtils.EMPTY,
                content.toPageContext());

        assertNotNull(renderText);
        assertEquals("No contributors found for: authors on selected page(s)", renderText);
    }

    public void testCustomErrorMessageEncoded() throws MacroException
    {
        final String unencodedScript = "<script>alert('bug')</script>";
        
        String renderText = macro.execute(
                new HashMap<String, String>()
                {
                    {
                        put("noneFoundMessage", unencodedScript);
                    }
                },
                StringUtils.EMPTY,
                content.toPageContext());
        assertEquals(GeneralUtil.htmlEncode(unencodedScript), renderText);
    }

    public void testNoContributorsFoundMessageEncoded() throws MacroException
    {
        final String unencodedScript = "<script>alert('bug')</script>";
        
        String renderText = macro.execute(
                new HashMap<String, String>()
                {
                    {
                        put("include", unencodedScript);
                    }
                },
                StringUtils.EMPTY,
                content.toPageContext());
        assertEquals("No contributors found for: " + GeneralUtil.htmlEncode(unencodedScript) + " on selected page(s)", renderText);
    }

    public void testExecuteGreedy()
    {
        final String renderText = macro.execute(
                new HashMap<String, String>()
                {
                    {
                        put("showPages", Boolean.TRUE.toString());
                        put("include", "authors,labels,comments,watches");
                    }
                },
                StringUtils.EMPTY,
                content.toPageContext());

        assertTrue(null != renderText);
        assertFalse(StringUtils.EMPTY.equals(renderText));
    }
}