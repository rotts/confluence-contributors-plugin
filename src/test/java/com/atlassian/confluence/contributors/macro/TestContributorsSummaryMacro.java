package com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.contributors.util.AbstractTest;
import com.atlassian.confluence.contributors.util.AuthorRankingSystem;
import com.atlassian.confluence.contributors.util.PageDetailsHelper;
import com.atlassian.confluence.contributors.util.PageSearchHelper;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;
import org.jmock.Mock;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestContributorsSummaryMacro extends AbstractTest
{
    private Map<String, String> macroParameters;

    private ContentEntityObject content;

    private RenderContext renderContext;

    private ContributorsSummaryMacro macro;

    private Mock mockSettingsManager;

    public void setUp() throws Exception
    {
        super.setUp();

        mockSettingsManager = mock(SettingsManager.class);

        Settings globalSettings = new Settings();
        globalSettings.setBaseUrl("http://localhost:1990/confluence");
        mockSettingsManager.expects(atLeastOnce()).method("getGlobalSettings").withNoArguments().will(returnValue(globalSettings));


        setUpPage();
        setUpMacroParameters();
        setUpRenderContext();
        setUpMacro();
    }

    private void setUpMacro()
    {
        macro = new ContributorsSummaryMacro(
                null,
                null,
                (SettingsManager) mockSettingsManager.proxy(),
                new PageSearchHelper()
                {
                    public List<AbstractPage> pageList(String pageName, String spaceKey, String labels, String scope, Calendar publishDate, String contentType)
                    {
                        return Collections.emptyList();
                    }
                },
                new PageDetailsHelper()
                {
                    public void processPageList(List<AbstractPage> pageList, AuthorRankingSystem rankingSystem, int groupBy)
                    {
                    }

                    public void processPageList(List<AbstractPage> pages, AuthorRankingSystem rankingSystem, int groupBy, boolean contributors, boolean comments, boolean labels, boolean watches)
                    {
                    }
                })
        {

            @Override
            protected String generateTableId()
            {
                return "0";
            }

            @Override
            public String getText(final String key)
            {
                return key;
            }
        };
    }

    private void setUpPage()
    {
        final Space pageSpace;

        content = new Page();
        content.setTitle("Test Page");

        pageSpace = new Space();
        pageSpace.setKey("DS");

        ((AbstractPage) content).setSpace(pageSpace);
    }

    private void setUpRenderContext()
    {
        renderContext = new PageContext(getContent());
    }

    private void setUpMacroParameters()
    {
        macroParameters = new HashMap<String, String>();
    }

    public Map<String, String> getMacroParameters()
    {
        return macroParameters;
    }

    public RenderContext getRenderContext()
    {
        return renderContext;
    }

    public ContentEntityObject getContent()
    {
        return content;
    }

    public void testExecuteDefault()
    {
        final Map macroParameters = getMacroParameters();

        macroParameters.clear();


        final String renderText = macro.execute(getMacroParameters(), StringUtils.EMPTY, getRenderContext());

        assertTrue(null != renderText);
        assertTrue(renderText.endsWith("</table>"));
    }

    public void testExecuteGreedy()
    {
        final Map<String, String> macroParameters = getMacroParameters();

        macroParameters.clear();
        macroParameters.put(
                "columns",
                "comments, commented"
                        + ", edits, edited"
                        + ", labels, labeled"
                        + ", labellist, lastupdate"
                        + ", watches, watching");


        final String renderText = macro.execute(getMacroParameters(), StringUtils.EMPTY, getRenderContext());

        assertTrue(null != renderText);
        assertTrue(renderText.endsWith("</table>"));
    }
}