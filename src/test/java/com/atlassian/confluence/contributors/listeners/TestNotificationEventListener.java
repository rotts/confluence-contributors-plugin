package com.atlassian.confluence.contributors.listeners;

import com.atlassian.bonnie.Indexer;
import com.atlassian.confluence.event.events.admin.ConfluenceReadyEvent;
import com.atlassian.confluence.event.events.content.mail.notification.ContentNotificationAddedEvent;
import com.atlassian.confluence.event.events.content.mail.notification.ContentNotificationRemovedEvent;
import com.atlassian.confluence.event.events.content.mail.notification.NotificationEvent;
import com.atlassian.confluence.event.events.content.mail.notification.SpaceNotificationAddedEvent;
import com.atlassian.confluence.event.events.content.mail.notification.SpaceNotificationRemovedEvent;
import com.atlassian.confluence.mail.notification.DefaultNotificationManager;
import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.ConfluenceIndexer;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.event.Event;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.util.Arrays;
import java.util.List;

public class TestNotificationEventListener extends MockObjectTestCase
{
    private Mock mockIndexer;

    private Object eventSource;

    private Notification notification;

    private NotificationEventListener notificationEventListener;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockIndexer = new Mock(ConfluenceIndexer.class);

        eventSource = new DefaultNotificationManager();

        notification = new Notification();

        notificationEventListener = new NotificationEventListener((ConfluenceIndexer) mockIndexer.proxy());
    }

    public void testIndexingTriggerWhenPageWatchAdded()
    {
        final AbstractPage pageToBeReindexed = new Page();
        final Event event;

        notification.setPage(pageToBeReindexed);
        event = new ContentNotificationAddedEvent(eventSource, notification);

        mockIndexer.expects(once()).method("reIndex").with(same(pageToBeReindexed));

        notificationEventListener.handleEvent(event);
    }

    public void testIndexingTriggerWhenPageWatchRemoved()
    {
        final AbstractPage pageToBeReindexed = new Page();
        final Event event;

        notification.setPage(pageToBeReindexed);
        event = new ContentNotificationRemovedEvent(eventSource, notification);

        mockIndexer.expects(once()).method("reIndex").with(same(pageToBeReindexed));

        notificationEventListener.handleEvent(event);
    }

    public void testIndexingTriggerWhenBlogPostWatchAdded()
    {
        final AbstractPage pageToBeReindexed = new Page();
        final Event event;

        notification.setPage(pageToBeReindexed);
        event = new ContentNotificationAddedEvent(eventSource, notification);

        mockIndexer.expects(once()).method("reIndex").with(same(pageToBeReindexed));

        notificationEventListener.handleEvent(event);
    }

    public void testIndexingTriggerWhenBlogPostWatchRemoved()
    {
        final AbstractPage pageToBeReindexed = new Page();
        final Event event;

        notification.setPage(pageToBeReindexed);
        event = new ContentNotificationRemovedEvent(eventSource, notification);

        mockIndexer.expects(once()).method("reIndex").with(same(pageToBeReindexed));

        notificationEventListener.handleEvent(event);
    }

    public void testIndexingTriggerWhenSpaceWatchAdded()
    {
        final Space spaceToBeReindexed = new Space();
        final Event event;

        notification.setSpace(spaceToBeReindexed);
        event = new SpaceNotificationAddedEvent(eventSource, notification);

        mockIndexer.expects(once()).method("reIndex").with(same(spaceToBeReindexed));

        notificationEventListener.handleEvent(event);
    }

    public void testIndexingTriggerWhenSpaceWatchRemoved()
    {
        final Space spaceToBeReindexed = new Space();
        final Event event;

        notification.setSpace(spaceToBeReindexed);
        event = new SpaceNotificationRemovedEvent(eventSource, notification);

        mockIndexer.expects(once()).method("reIndex").with(same(spaceToBeReindexed));

        notificationEventListener.handleEvent(event);
    }

    public void testIndexingNotTriggerWhenOnNonSpaceOrAbstractPageNotificationEvents()
    {
        final Event event = new ConfluenceReadyEvent(eventSource);
        notificationEventListener.handleEvent(event);
    }

    public void testHandledEventsLimitedToNotifications()
    {
        final List<Class> handledClasses = Arrays.asList(notificationEventListener.getHandledEventClasses());
        final List<Class<? extends NotificationEvent>> expectedHandledClasses = Arrays.asList(
                ContentNotificationAddedEvent.class,
                ContentNotificationRemovedEvent.class,
                SpaceNotificationAddedEvent.class,
                SpaceNotificationRemovedEvent.class
        );

        assertEquals(expectedHandledClasses.size(), handledClasses.size());
        assertTrue(handledClasses.containsAll(expectedHandledClasses));
    }
}
