package it.com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class ContributorsSummaryTestCase extends AbstractContributorsTestCase {

    public void testContributorsSummaryCalculationWithPageMarkedAsFavoriteByOtherUsers() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);
        assertTrue(pageHelper.read());

        
        
        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "jane.doe", "3", pageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-01", "0", StringUtils.EMPTY },
                        { "Admin", "2", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "~admin:favourite", "1", pageHelper.getTitle() },
                        { "john.doe", "2", pageHelper.getTitle(), "1", pageHelper.getTitle(), "1", "label-johndoe-01", "1", pageHelper.getTitle() }
                });
    }

    public void testContributorsSummaryCalculationWithPageMarkedAsFavoriteBySelf() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        assertTrue(pageHelper.read());

        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "jane.doe", "3", pageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-01", "0", StringUtils.EMPTY },
                        { "Admin", "2", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "my:favourite", "1", pageHelper.getTitle() },
                        { "john.doe", "2", pageHelper.getTitle(), "1", pageHelper.getTitle(), "1", "label-johndoe-01", "1", pageHelper.getTitle() }
                });
    }

    public void testContributorsSummaryCalculationOrderedByName() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|order=name}");
        assertTrue(pageHelper.update());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "Admin", "3", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "my:favourite", "1", pageHelper.getTitle() },
                        { "jane.doe", "3", pageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-01", "0", StringUtils.EMPTY },
                        { "john.doe", "2", pageHelper.getTitle(), "1", pageHelper.getTitle(), "1", "label-johndoe-01", "1", pageHelper.getTitle() }
                });
    }

    private void editPageAs(final String userName) {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        logout();
        login(userName, userName);

        assertTrue(pageHelper.read());

        pageHelper.setContent(
                pageHelper.getContent() + "\n" +
                "Edited by: " + userName);
        assertTrue(pageHelper.update());
    }

    public void testContributorsSummaryCalculationOrderedByLastEdit() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        editPageAs(NAMES_OF_USERS_TO_CREATE[1]); /* Edit as jane.doe */
        editPageAs(NAMES_OF_USERS_TO_CREATE[0]); /* Edit as john.doe */

        logout();
        loginAsAdmin();

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|order=editTime}");
        assertTrue(pageHelper.update());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "Admin", "3", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "my:favourite", "1", pageHelper.getTitle() },
                        { "john.doe", "3", pageHelper.getTitle(), "1", pageHelper.getTitle(), "1", "label-johndoe-01", "1", pageHelper.getTitle() },
                        { "jane.doe", "4", pageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-01", "0", StringUtils.EMPTY }
                });
    }

    public void testContributorsSummaryCalculationReversed() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|reverse=true}");
        assertTrue(pageHelper.update());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "john.doe", "2", pageHelper.getTitle(), "1", pageHelper.getTitle(), "1", "label-johndoe-01", "1", pageHelper.getTitle() },
                        { "Admin", "2", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "~admin:favourite", "1", pageHelper.getTitle() },
                        { "jane.doe", "4", pageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-01", "0", StringUtils.EMPTY }
                });
    }

    public void testContributorsSummaryCalculationLimited() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|limit=2}");
        assertTrue(pageHelper.update());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "jane.doe", "4", pageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-01", "0", StringUtils.EMPTY },
                        { "Admin", "2", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "~admin:favourite", "1", pageHelper.getTitle() }
                });
    }

    public void testContributorsSummaryCalculationIncludingOtherPagesWhichHaveCommonLabels() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final PageHelper anotherPageHelper = getPageHelper();

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|labels=label-janedoe-01}");
        assertTrue(pageHelper.update());

        /* Create another page with labels label-janedoe-01 (existing) and label-janedoe-02 (new) */
        anotherPageHelper.setSpaceKey(testSpaceKey);
        anotherPageHelper.setTitle("Another page with label label-janedoe-01");
        anotherPageHelper.setLabels(Arrays.asList(new String[] { "label-janedoe-01", "label-janedoe-02" }));
        assertTrue(anotherPageHelper.create());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        /* We want to see if the newly created page is included in the calculation */
        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "jane.doe", "5", anotherPageHelper.getTitle() + " " + pageHelper.getTitle(), "0", StringUtils.EMPTY, "3", "label-janedoe-01 label-janedoe-02", "1", anotherPageHelper.getTitle() },
                        { "Admin", "2", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "~admin:favourite", "1", pageHelper.getTitle() },
                        { "john.doe", "2", pageHelper.getTitle(), "1", pageHelper.getTitle(), "1", "label-johndoe-01", "1", pageHelper.getTitle() }
                });
    }

    public void testContributorsSummaryCalculationOfOnlyPagesWithOneSpecificLabel() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final PageHelper anotherPageHelper = getPageHelper();

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|labels=label-janedoe-02}");
        assertTrue(pageHelper.update());

        /* Create another page with labels label-janedoe-01 (existing) and label-janedoe-02 (new) */
        anotherPageHelper.setSpaceKey(testSpaceKey);
        anotherPageHelper.setTitle("Another page with label label-janedoe-01");
        anotherPageHelper.setLabels(Arrays.asList(new String[] { "label-janedoe-02" }));
        assertTrue(anotherPageHelper.create());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        /* We want to see if only the new page is included in the result */
        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "jane.doe", "1", anotherPageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-02", "1", anotherPageHelper.getTitle() },
                });
    }

    public void testContributorsSummaryCalculationOfBlogPostsOfSpecificPublishedDate() throws ParseException
    {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final BlogPostHelper blogPostHelper = getBlogPostHelper();

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|publishDate=2004/11/21}");
        assertTrue(pageHelper.update());

        /* Create a blog post which has a creation date of Nov 21, 2004 */
        blogPostHelper.setSpaceKey(testSpaceKey);
        blogPostHelper.setTitle(pageHelper.getTitle());
        blogPostHelper.setCreationDate(new SimpleDateFormat("yyyy/MM/dd").parse("2004/11/21"));
        blogPostHelper.setContent("Test Blog Content");
        assertTrue(blogPostHelper.create());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        /* We want to see if only the new page is included in the result */
        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "jane.doe", "5", blogPostHelper.getTitle() + " " + pageHelper.getTitle(), "0", StringUtils.EMPTY, "1", "label-janedoe-01", "0", StringUtils.EMPTY },
                        { "Admin", "2", pageHelper.getTitle(), "2", pageHelper.getTitle(), "1", "~admin:favourite", "1", pageHelper.getTitle() },
                        { "john.doe", "2", pageHelper.getTitle(), "1", pageHelper.getTitle(), "1", "label-johndoe-01", "1", pageHelper.getTitle() }
                });
    }

    public void testContributorsSummaryCalculationShowingAnonymousUserContributions() {
        final PageHelper pageHelper = getPageHelper();
        final long confluenceOverviewPageId;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");

        confluenceOverviewPageId = pageHelper.findBySpaceKeyAndTitle();
        assertTrue(0 < confluenceOverviewPageId);

        pageHelper.setId(confluenceOverviewPageId);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching|showAnonymous=true|order=name}");
        assertTrue(pageHelper.update());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTableEquals("Contributors Summary Table",
                new String[][] {
                        { "User", "Edits", "Edited", "Comments", "Commented", "Labels", "Label List", "Watches", "Watching" },
                        { "Admin", "1", pageHelper.getTitle(), "0", StringUtils.EMPTY, "0", StringUtils.EMPTY, "0", StringUtils.EMPTY },
                        { "Anonymous", "3", pageHelper.getTitle(), "1", pageHelper.getTitle(), "0", StringUtils.EMPTY, "0", StringUtils.EMPTY },
                });
    }

    /*
     * CONTRB-45 - If test fails in Confluence 4.0, please see http://jira.atlassian.com/browse/CONFDEV-1370
     */
    public void testViewContributorsAndContributorsSummaryInHistoricPage() throws IOException {
        final File copyOfContrb45TestData = ClasspathResourceUtil.getClassPathResourceAsTempFile("contrb-45-site-export.zip", getClass().getClassLoader(), ".zip");

        try
        {
            getConfluenceWebTester().restoreData(copyOfContrb45TestData);

            gotoPage("/display/TST/CONTRB-45");

            clickLink("action-view-history-link");

            clickLinkWithText("v. 1");

            assertTextPresent("old version");
            assertTextNotPresent("Error formatting macro: contributors-summary: java.lang.IllegalArgumentException: spaceKey cannot be null");
            assertTextNotPresent("Error formatting macro: contributors: java.lang.IllegalArgumentException: spaceKey cannot be null");
        }
        finally
        {
            copyOfContrb45TestData.delete();
        }
    }

    public void testAuthorFullNamesHtmlEncoded() throws IOException
    {
        File testData = ClasspathResourceUtil.getClassPathResourceAsTempFile("contrib-52-test-data.zip", getClass().getClassLoader(), ".zip");
        try
        {
            getConfluenceWebTester().restoreData(testData);

            logout();
            login("jdoe", "jdoe");

            PageHelper pageHelper = getPageHelper();
            pageHelper.setSpaceKey("DS");
            pageHelper.setTitle("testAuthorFullNamesHtmlEncoded");
            pageHelper.setContent("{contributors-summary}");

            assertTrue(pageHelper.create());

            logout();
            loginAsAdmin();

            getIndexHelper().update();

            gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertEquals(
                    "user1 <script>alert('Whoho!'); </script>",
                    getElementTextByXPath("//table[@summary='Contributors Summary Table']//td[1]//a")
            );
        }
        finally
        {
            if (null != testData && testData.isFile())
                assertTrue(testData.delete());
        }
    }

}
