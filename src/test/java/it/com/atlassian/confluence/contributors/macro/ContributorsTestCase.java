package it.com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.util.ClasspathResourceUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

public class ContributorsTestCase extends AbstractContributorsTestCase {
    
    private static final String NEW_ADMIN_FULL_NAME = "Your Smooth Operator";

    protected void setUp() throws Exception {
        super.setUp();
        /* We need to do this because the admin's full name, "Admin", is far too general to use
         * for assertions in the response text.
         */
        updateAdminFullName();
    }

    private void updateAdminFullName() {
        gotoPage("/users/editmyprofile.action");

        setWorkingForm("editmyprofileform");
        setTextField("fullName", NEW_ADMIN_FULL_NAME);
        submit("confirm");
    }

    private String getRenderedContentOfContributedPage() {
        return getPageHelper(idOfPageWhichContainsContributions).getContentRendered();
    }

    public void testGetEditContributorsList() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];
        String responseText;
        

        logout();
        login(userNameJane, userNameJane);

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:include=authors}");
        assertTrue(pageHelper.update());

        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        responseText = getRenderedContentOfContributedPage();

        assertTrue(responseText.indexOf(userNameJane) >= 0);
        assertTrue(responseText.indexOf(userNameJohh) >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);

        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(userNameJohh));
        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(NEW_ADMIN_FULL_NAME));
        assertTrue(responseText.indexOf(userNameJohh) < responseText.indexOf(NEW_ADMIN_FULL_NAME));
    }

    public void testGetCommentContributorsList() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];
        String responseText;


        logout();
        login(userNameJane, userNameJane);

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:include=comments}");
        assertTrue(pageHelper.update());
        
        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        responseText = getRenderedContentOfContributedPage();

        /* Since jane.doe has not commented on anything, she won't appear in the page */
        assertTrue(-1 == responseText.indexOf(userNameJane));
        assertTrue(responseText.indexOf(userNameJohh) >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);


        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) < responseText.indexOf(userNameJohh));
    }

    private void addMoreLabelAsJohnDoe() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final List<String> labels;

        logout();
        login(NAMES_OF_USERS_TO_CREATE[0], NAMES_OF_USERS_TO_CREATE[0]);

        assertTrue(pageHelper.read());

        labels = new ArrayList<String>(pageHelper.getLabels());
        labels.add("label-johndoe-02");

        assertTrue(pageHelper.update());
    }

    private void addMoreLabelAsJaneDoe() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final List<String> labels;

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        assertTrue(pageHelper.read());

        labels = new ArrayList<String>(pageHelper.getLabels());
        labels.add("label-janedoe-02");
        labels.add("label-janedoe-03");

        assertTrue(pageHelper.update());
    }

    public void testGetLabelContributorsList() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];
        String responseText;

        addMoreLabelAsJohnDoe();
        addMoreLabelAsJaneDoe();


        logout();
        login(userNameJane, userNameJane);

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:include=labels}");
        assertTrue(pageHelper.update());

        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        responseText = getRenderedContentOfContributedPage();

        assertTrue(responseText.indexOf(userNameJane) >= 0);
        assertTrue(responseText.indexOf(userNameJohh) >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);

        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(userNameJohh));
        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(NEW_ADMIN_FULL_NAME));
        assertTrue(responseText.indexOf(userNameJohh) < responseText.indexOf(NEW_ADMIN_FULL_NAME));
    }

    private void addMoreWatchesAsJaneDoe() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final long confluenceOverviewPageId;

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        pageHelper.addWatch();
        assertTrue(pageHelper.isWatched());

        pageHelper.setId(0);
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");

        assertFalse(0 >= (confluenceOverviewPageId = pageHelper.findBySpaceKeyAndTitle()));

        pageHelper.setId(confluenceOverviewPageId);
        pageHelper.addWatch();
        assertTrue(pageHelper.isWatched());
    }

    public void testGetWatcherContributorsList() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];
        String responseText;

        addMoreWatchesAsJaneDoe();

        getIndexHelper().update();

        logout();
        login(userNameJane, userNameJane);

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:include=watches}");
        assertTrue(pageHelper.update());

        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        responseText = getRenderedContentOfContributedPage();

        assertTrue(responseText.indexOf(userNameJane) >= 0);
        assertTrue(responseText.indexOf(userNameJohh) >= 0);
        /* Since admin is the page creator, it'll automatically watch the page */
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);

        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(userNameJohh));
    }

    public void testGetContributorsListOrderedByName() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];
        String responseText;

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:order=name}");
        assertTrue(pageHelper.update());

        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        responseText = getRenderedContentOfContributedPage();

        assertTrue(responseText.indexOf(userNameJane) >= 0);
        assertTrue(responseText.indexOf(userNameJohh) >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);

        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(userNameJohh));
        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(NEW_ADMIN_FULL_NAME));
        assertTrue(responseText.indexOf(userNameJohh) < responseText.indexOf(NEW_ADMIN_FULL_NAME));
    }

    private void editPageAs(final String userName) {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        logout();
        login(userName, userName);

        assertTrue(pageHelper.read());

        pageHelper.setContent(
                pageHelper.getContent() + "\n" +
                "Edited by: " + userName);
        assertTrue(pageHelper.update());
    }

    public void testGetContributorsListOrderedByUpdate() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];

        editPageAs(NAMES_OF_USERS_TO_CREATE[1]); /* Edit as jane.doe */
        editPageAs(NAMES_OF_USERS_TO_CREATE[0]); /* Edit as john.doe */

        logout();
        loginAsAdmin();

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:order=update}");
        assertTrue(pageHelper.update());

        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        assertEquals(userNameJane, getElementTextByXPath("//div[@class='plugin-contributors']/span/span/a[3]"));
        assertEquals(userNameJohh, getElementTextByXPath("//div[@class='plugin-contributors']/span/span/a[2]"));
        assertEquals(NEW_ADMIN_FULL_NAME, getElementTextByXPath("//div[@class='plugin-contributors']/span/span/a[1]"));

//        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) < responseText.indexOf(userNameJohh));
//        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) < responseText.indexOf(userNameJane));
//        assertTrue(responseText.indexOf(userNameJohh) < responseText.indexOf(userNameJane));
    }
    
    public void testGetReversedContributorsList() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];
        String responseText;


        logout();
        login(userNameJane, userNameJane);

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:include=authors|reverse=true}");
        assertTrue(pageHelper.update());

        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        responseText = getRenderedContentOfContributedPage();

        assertTrue(responseText.indexOf(userNameJane) >= 0);
        assertTrue(responseText.indexOf(userNameJohh) >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);

        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) < responseText.indexOf(userNameJohh));
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) < responseText.indexOf(userNameJane));
        assertTrue(responseText.indexOf(userNameJohh) < responseText.indexOf(userNameJane));
    }

    public void testGetLimitedContributorsList() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final String userNameJohh = NAMES_OF_USERS_TO_CREATE[0];
        final String userNameJane = NAMES_OF_USERS_TO_CREATE[1];
        String responseText;


        logout();
        login(userNameJane, userNameJane);

        assertTrue(pageHelper.read());

        pageHelper.setContent("{contributors:include=authors|limit=2}");
        assertTrue(pageHelper.update());

        getIndexHelper().flush();

        gotoPage("/pages/viewpage.action?pageId=" + idOfPageWhichContainsContributions);

        assertLinkPresentWithText("..."); /* For the hidden admin user */

        responseText = getRenderedContentOfContributedPage();

        assertTrue(responseText.indexOf(userNameJane) >= 0);
        assertTrue(responseText.indexOf(userNameJohh) >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);

        assertTrue(responseText.indexOf(userNameJane) < responseText.indexOf(userNameJohh));
    }

    public void testGetLimitedContributorsListShowingAnonymousUserContributions() {
        final PageHelper pageHelper = getPageHelper();
        final long confluenceOverviewPageId;
        final String responseText;

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Confluence Overview");

        confluenceOverviewPageId = pageHelper.findBySpaceKeyAndTitle();
        assertTrue(0 < confluenceOverviewPageId);

        pageHelper.setId(confluenceOverviewPageId);

        assertTrue(pageHelper.read());
        pageHelper.setContent("{contributors:showAnonymous=true|order=name}");
        assertTrue(pageHelper.update());

        getIndexHelper().update();

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        responseText = pageHelper.getContentRendered();

        assertTrue(responseText.indexOf("Anonymous") >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) >= 0);
        assertTrue(responseText.indexOf(NEW_ADMIN_FULL_NAME) > responseText.indexOf("Anonymous"));
    }

    public void testUserFullNamesHtmlEncoded() throws IOException
    {
        File contrib54TestData = ClasspathResourceUtil.getClassPathResourceAsTempFile("contrib-54-site-export.zip", getClass().getClassLoader(), ".zip");
        try
        {
            getConfluenceWebTester().restoreData(contrib54TestData);
            gotoPage("/display/ds/CONTRB-54");
            assertLinkPresentWithText("<script>alert('vulnerable')</script>");
        }
        finally
        {
            if (null != contrib54TestData && contrib54TestData.isFile())
                contrib54TestData.delete();
        }
    }

    public void testUserFullNamesHtmlEncodedInListMode() throws IOException
    {
        File contrib54TestData = ClasspathResourceUtil.getClassPathResourceAsTempFile("contrib-54-site-export.zip", getClass().getClassLoader(), ".zip");
        try
        {
            getConfluenceWebTester().restoreData(contrib54TestData);

            PageHelper pageHelper = getPageHelper();
            pageHelper.setSpaceKey("ds");
            pageHelper.setTitle("CONTRB-54");
            pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());
            assertTrue(pageHelper.read());

            pageHelper.setContent("{contributors:mode=list}");
            assertTrue(pageHelper.update());

            getIndexHelper().flush();

            gotoPage("/display/ds/CONTRB-54");
            
            // Just to make sure it's the list mode being tested
            assertElementPresentByXPath("//div[@class='plugin-contributors']//ul[@class='contributors-list']");
            assertLinkPresentWithText("<script>alert('vulnerable')</script>");
        }
        finally
        {
            if (null != contrib54TestData && contrib54TestData.isFile())
                contrib54TestData.delete();
        }
    }
}
