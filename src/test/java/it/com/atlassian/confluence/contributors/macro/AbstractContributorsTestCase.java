package it.com.atlassian.confluence.contributors.macro;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.CommentHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractContributorsTestCase extends AbstractConfluencePluginWebTestCase {

    protected static final String[] NAMES_OF_USERS_TO_CREATE =
            new String[] { "john.doe", "jane.doe" };

    protected String testSpaceKey;

    protected long idOfPageWhichContainsContributions;

    private void updateAdminFullName() {
        gotoPage("/users/editmyprofile.action");

        setWorkingForm("editmyprofileform");
        setTextField("fullName", "Admin");
        submit("confirm");
    }

    protected void createUser(final String name) {
        final UserHelper userHelper = getUserHelper();

        userHelper.setName(name);
        userHelper.setFullName(name);
        userHelper.setEmailAddress(name + "@localhost.localdomain");
        userHelper.setPassword(name);
        userHelper.setGroups(Arrays.asList("confluence-users", "confluence-administrators"));

        assertTrue(userHelper.create());
    }

    protected void createSpace() {
        final SpaceHelper spaceHelper = getSpaceHelper();

        spaceHelper.setKey(testSpaceKey = "tst");
        spaceHelper.setName("Test Space");
        spaceHelper.setType(SpaceHelper.TYPE_GLOBAL);
        assertTrue(spaceHelper.create());
    }

    protected void createPageToBeContributedByUsers() {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Contribute to me!");
        pageHelper.setContent("Edited by: " + getConfluenceWebTester().getCurrentUserName() + "\n");

        assertTrue(pageHelper.create());

        idOfPageWhichContainsContributions = pageHelper.getId();
    }

    protected void createAdminContributions() {
        final CommentHelper commentHelper = getCommentHelper();
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);

        /* First comment */
        commentHelper.setContentId(idOfPageWhichContainsContributions);
        commentHelper.setContent(getConfluenceWebTester().getCurrentUserName());
        assertTrue(commentHelper.create());

        /* Second comment */
        commentHelper.setParentId(commentHelper.getId());
        commentHelper.setId(0);
        assertTrue(commentHelper.create());

        assertTrue(pageHelper.read());

        pageHelper.markFavourite();

        assertTrue(pageHelper.update());
    }

    protected void createJohnDoeContributions() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final List<String> pageLabels;
        final CommentHelper commentHelper;

        logout();
        login(NAMES_OF_USERS_TO_CREATE[0], NAMES_OF_USERS_TO_CREATE[0]);

        assertTrue(pageHelper.read());

        /* First comment */
        commentHelper = getCommentHelper();
        commentHelper.setContentId(idOfPageWhichContainsContributions);
        commentHelper.setContent(getConfluenceWebTester().getCurrentUserName());
        assertTrue(commentHelper.create());

        /* First edit */
        pageHelper.setContent(
                pageHelper.getContent() +
                "Edited by: " + getConfluenceWebTester().getCurrentUserName() + "\n");
        assertTrue(pageHelper.update());

        /* Second edit */
        pageLabels = new ArrayList<String>(pageHelper.getLabels());
        pageLabels.add("label-johndoe-01");
        pageHelper.setLabels(pageLabels);

        pageHelper.setContent(
                pageHelper.getContent() +
                "Edited by: " + getConfluenceWebTester().getCurrentUserName() + "\n");

        assertTrue(pageHelper.update());

        pageHelper.addWatch();
        assertTrue(pageHelper.isWatched());
    }

    protected void createJaneDoeContributions() {
        final PageHelper pageHelper = getPageHelper(idOfPageWhichContainsContributions);
        final List<String> pageLabels;

        logout();
        login(NAMES_OF_USERS_TO_CREATE[1], NAMES_OF_USERS_TO_CREATE[1]);

        assertTrue(pageHelper.read());

        /* First edit */
        pageHelper.setContent(
                pageHelper.getContent() +
                "Edited by: " + getConfluenceWebTester().getCurrentUserName() + "\n");
        assertTrue(pageHelper.update());

        /* Second edit */
        pageHelper.setContent(
                pageHelper.getContent() +
                "Edited by: " + getConfluenceWebTester().getCurrentUserName() + "\n");
        assertTrue(pageHelper.update());

        /* Third edit */
        pageLabels = new ArrayList<String>(pageHelper.getLabels());
        pageLabels.add("label-janedoe-01");
        pageHelper.setLabels(pageLabels);

        pageHelper.setContent("{contributors-summary:columns=edits,edited,comments,commented,labels,labellist,watches,watching}");
        assertTrue(pageHelper.update());
    }

    protected void createContributions() {
        createAdminContributions();
        createJohnDoeContributions();
        createJaneDoeContributions();
    }

    protected void setUp() throws Exception {
        super.setUp();

        updateAdminFullName();
        refreshLicense();

        for (String userName:  NAMES_OF_USERS_TO_CREATE)
            createUser(userName);

        createSpace();
        createPageToBeContributedByUsers();
        createContributions();


        logout();
        loginAsAdmin();

        getIndexHelper().update();
    }
}
